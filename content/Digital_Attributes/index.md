![Architecture Office](/static/images/dxclogo.png)

# DXC Digital Attributes

As DXC focus on developing our Digital Offering or Solutions for our clients we should look to outline the attributes which need to be considered that make up an Digital Offering or Solution. It is important that we consider this as the market and clients will look and assess DXC Digital Capability against our marketed portfolio and solution development for our clients.

Digital Offerings and Solution should be considered in the following dimensions

-	Functionality (Platforms, Technology and Commercials)
-	Design
-	Development
-	Operations

### Functionality

If we take the view that a Digital Business is one that is 'Information Driven' the offerings and solutions that underpin this should be enabling the client to <b>Create, Host, Analyse and Use</b> the Information from it's systems and services to support this direction.

<b>Platforms</b> - The term Platform has become commonly used in the discussion around digital. A Digital Platform is now considered a key enabler to businesses. Digital Platforms make up the <b>Digital Core</b>, <b>Digital Context</b> is created and or used from the platforms.

![Digital Context & Core](/static/images/contextcore.png)

Below are 8 attributes which outline the principles of a Digital Platform. These attributes have been taken from an external source [LINK](http://stephane-castellani.com/everything-you-need-to-know-about-digital-platforms/). In time we would expect DXC to refine these to a more concise set for our business strategy.

1: It is a technology-enabled business model.

2: It facilitates exchanges between multiple groups – for example end users and producers – who don’t necessarily know each other.

3: It offers a value that is proportional to the size of the community. There are network effects. A Digital Platform is worth nothing without its community.

4: It is a trust enabler : it must generate trust with clear general terms and conditions regarding the intellectual property and data ownership. It also helps consumers and providers to trust each other within the network thanks to scoring mechanisms.

5: It has open connectivity : it shares data with 3rd party developers to create new services and extend the ecosystem. This is done with APIs and participates to the API economy.

6: It can scale massively to address millions of consumers without performance degradation.

7: It offers compelling user experience: easy to use, no training needs, self-service.

8: It has innovative business models based on the immediate value.

<b>What are the benefits of a Digital Platform?</b>

- Generate revenue
- Reduce costs
- Foster collaboration and innovation for new products and services
- Gain speed to put products on the targeted markets.

<b>Technologies/Systems</b> - As we look at Digital Offerings / Solutions the technologies which make them up are important. New technology like but not limited to Cloud, IoT, RPA, AI, Mobile, AR/VR.

The offering / Service should also exhibit key features and functions like Automated request / deployment and configuration [See Operations](#-operations).

<b>Commercial</b> - This can be considered at times, although it is not a primary factor. A commercial construct which supports 'As a Service' / Utility style charging can be an aspect of Cloud Services and therefore Utility / consumption based charging models needs to be factored in.

### Design

The Design is an important part of an Offering or Solution.  Designs should reflect a loosely coupled / Micro service-based architecture, enabled by API / Protocol based integration.  Offerings / Solution should be broken down into small business focused functions, data sources should not be shared.  Design should consider the Development and Operations aspects, as to support CI/CD and Optimised Operations the design has to cater for these from day one.

For more detail on DXC Design and Development Principles please follow this [LINK](https://github.dxc.com/pages/ArchitectureOffice/Standards/Design-Principles/)

### Development

Offering / Solutions should be developed using modern agile techniques, supporting small incremental sprints of improvement. There should be a defined Product Owner with a supporting development team [High Performing Teams](https://github.dxc.com/pages/ArchitectureOffice/Standards/High_Performing_Teams/) that includes Development and Operational resources.  Teams should be highly collaborative, and ensuring iterative testing and automated deployment capabilities of releases.

For more detail on DXC Design and Development Principles please follow this [LINK](https://github.dxc.com/pages/ArchitectureOffice/Standards/Design-Principles/)

### Operations

A Digital Offering / Solution should exhibit certain operational characteristics, these include highly automated including Deployment, Configuration and Run and Maintain. The ability to extract information about the operation of the Offering / Solution should be possible to support analysis either real-time response or long-term trending to help with feedback for continuous improvement.

The extraction of Information from the system should enable more robust reporting on the operation of the solution, which will include predictive analytics reporting on potential upcoming errors or problems before events occur. Billing, Capacity, Incidents, Threats and Configuration should all be handled as part of the reporting of the solution. Digital Solutions, should have a high degree of Machine Based operations for reporting.

Refer to High Performing Operations Page [LINK](https://github.dxc.com/pages/ArchitectureOffice/Standards/High_Performing_Operations/TopCharacteristics-2/)
