
# Pattern - Decompose by Subdomain

![Architecture Office](/static/images/dxclogo.png)

### Context
You are developing a large, complex application and want to use the microservice architecture. The microservice architecture structures an application as a set of loosely coupled services. The goal of the microservice architecture is to accelerate software development by enabling continuous delivery/deployment.

![Success Triangle](/static/images/successtriangle.png)

The microservice architecture does this in two ways:

Simplifies testing and enables components to deployed independently
Structures the engineering organization as a collection of small (6-10 members), autonomous teams, each of which is responsible for one or more services
These benefits are not automatically guaranteed. Instead, they can only be achieved by the careful functional decomposition of the application into services.

A service must be small enough to be developed by a small team and to be easily tested. A useful guideline from object-oriented design (OOD) is the Single Responsibility Principle (SRP). The SRP defines a responsibility of a class as a reason to change, and states that a class should only have one reason to change. It make sense to apply the SRP to service design as well and design services that are cohesive and implement a small set of strongly related functions.

The application also be decomposed in a way so that most new and changed requirements only affect a single service. That is because changes that affect multiple services requires coordination across multiple teams, which slows down development. Another useful principle from OOD is the Common Closure Principle (CCP), which states that classes that change for the same reason should be in the same package. Perhaps, for instance, two classes implement different aspects of the same business rule. The goal is that when that business rule changes developers, only need to change code in a small number - ideally only one - of packages. This kind of thinking makes sense when designing services since it will help ensure that each change should impact only one service.

### Problem
How to decompose an application into services?

### Forces
- The architecture must be stable
- Services must be cohesive. A service should implement a small set of strongly related functions.
- Services must conform to the Common Closure Principle - things that change together should be packaged together - to ensure that each change affect only one service
- Services must be loosely coupled - each service as an API that encapsulates its implementation. The implementation can be changed without affecting clients
- A service should be testable
- Each service be small enough to be developed by a “two pizza” team, i.e. a team of 6-10 people
- Each team that owns one or more services must be autonomous. A team must be able to develop and deploy their services with minimal collaboration with other teams.

### Solution
Define services corresponding to Domain-Driven Design (DDD) subdomains. DDD refers to the application’s problem space - the business - as the domain. A domain is consists of multiple subdomains. Each subdomain corresponds to a different part of the business.

Subdomains can be classified as follows:

- Core - key differentiator for the business and the most valuable part of the application
- Supporting - related to what the business does but not a differentiator. These can be implemented in-house or outsourced.
- Generic - not specific to the business and are ideally implemented using off the shelf software

### Examples
The subdomains of an online store include:

- Product catalog
- Inventory management
- Order management
- Delivery management
…
The corresponding microservice architecture would have services corresponding to each of these subdomains.

![Decompose by Sub Domain](/static/images/decomsub.png)

- Resulting Context
This pattern has the following benefits:

- Stable architecture since the subdomains are relatively stable
- Development teams are cross-functional, autonomous, and organized around delivering business value rather than technical features
- Services are cohesive and loosely coupled

### Anti-Corruption Layers
When a system needs to integrate with a legacy system or one beyond the boundary of organisational control (e.g. integration with a third-party logistics platform), the architecture can be decoupled from the legacy/external system’s conceptual model or API by proxying with a microservice. In Domain-Driven Design, Eric Evans described components that play this role as anti-corruption layers. Their purpose is to allow the integration of two systems without allowing the domain model of one system to corrupt the domain model of the other. These components are responsible for solving three problems:

- System integration
- Protocol translation
- Model translation

### Core, Generic and Support Sub Domains.

When looking at breaking down the subdomains, it helps to consider the characteristics of the domain, some practitioners apply three function labels to

<b>Core</b>, that represents the main part of the Domain. In this case, I think that the "Product Catalog" Subdomain is the Core because it is what customers will be interacting and, therefore, is from where the revenue will come (customer shopping). Others Subdomains were classified as Support and Generic.

<b>Support</b> Subdomains are like auxiliary ones. In practice, you set a Bounded Context to it and create a specific application. It will work as a support application for the Core Domain application, however, support applications will have its own model.

<b>Generic</b> Subdomains are like support ones, but they have a strong particularity: they are so generic solutions that they could be used not only in the Domain it was created, but they could be used by others Domains too. It is completely reasonably that your "Access Control Application", when well designed, can be reused to support other Domains which not the e-Commerce, for instance. That is why they are called Generic.

### Issues
There are the following issues to address:

How to identify the subdomains? Identifying subdomains and hence services requires an understanding of the business. Like business capabilities, subdomains are identified by analysing the business and its organisational structure and identifying the different areas of expertise. Subdomains are best identified using an iterative process. Good starting points for identifying subdomains are:

organization structure - different groups within an organization might correspond to subdomains
high-level domain model - subdomains often have a key domain object

### Reference

- [Microservice Pattern](./Microservice-Pattern.md)

- [Decompose by Business Domain](./Decomposing-by-Business-Domains.md)

- [Decompose by Sub Domain](./Decomposing-by-Sub-Domains.md)

###### Information - This is a direct copy from [microservices.io](http://microservices.io/patterns/microservices.html) - This description was found to be very well aligned with DXC thinking.
