
# Pattern - Microservice Architecture

![Architecture Office](/static/images/dxclogo.png)

### Context
You are developing a server-side enterprise application. It must support a variety of different clients including desktop browsers, mobile browsers and native mobile applications. The application might also expose an API for 3rd parties to consume. It might also integrate with other applications via either web services or a message broker. The application handles requests (HTTP requests and messages) by executing business logic; accessing a database; exchanging messages with other systems; and returning a HTML/JSON/XML response. There are logical components corresponding to different functional areas of the application.

### Problem
What’s the application’s deployment architecture?

### Forces
- There is a team of developers working on the application
- New team members must quickly become productive
- The application must be easy to understand and modify
- You want to practice continuous deployment of the application
- You must run multiple copies of the application on multiple machines in order to satisfy scalability and availability requirements
- You want to take advantage of emerging technologies (frameworks, programming languages, etc.)

### Solution
Define an architecture that structures the application as a set of loosely coupled, collaborating services. This approach corresponds to the Y-axis of the Scale Cube. Each service implements a set of narrowly, related functions. For example, an application might consist of services such as the order management service, the customer management service etc.

Services communicate using either synchronous protocols such as HTTP/REST or asynchronous protocols such as AMQP. Services can be developed and deployed independently of one another. Each service has its own database in order to be decoupled from other services. Data consistency between services is maintained using the Saga pattern

### Examples

##### Fictitious e-commerce application
Let’s imagine that you are building an e-commerce application that takes orders from customers, verifies inventory and available credit, and ships them. The application consists of several components including the StoreFrontUI, which implements the user interface, along with some backend services for checking credit, maintaining inventory and shipping orders. The application consists of a set of services.

![Microservice Pattern](/static/images/msecom.png)

##### Conceptual View of Micro Service Architecture

As with other examples the focus for the Micro Service is the separation of the business functions, and communications between services is done through APIs. This include front and back office, creating isolation for the business functions to allow them to focus on their primary function, but also enabling the ability to enhance or even replace the service without impact to the rest of the IT estate.

![Microservice Stack](/static/images/microservicestack.png)

### Resulting context

##### Benefits
This solution has a number of benefits:

Enables the continuous delivery and deployment of large, complex applications.
Better testability - services are smaller and faster to test
Better deployability - services can be deployed independently
It enables you to organize the development effort around multiple, auto teams. It enables you to organize the development effort around multiple teams. Each (two pizza) team is owns and is responsible for one or more single service. Each team can develop, deploy and scale their services independently of all of the other teams.
Each microservice is relatively small
Easier for a developer to understand
The IDE is faster making developers more productive
The application starts faster, which makes developers more productive, and speeds up deployments
Improved fault isolation. For example, if there is a memory leak in one service then only that service will be affected. The other services will continue to handle requests. In comparison, one misbehaving component of a monolithic architecture can bring down the entire system.
Eliminates any long-term commitment to a technology stack. When developing a new service you can pick a new technology stack. Similarly, when making major changes to an existing service you can rewrite it using a new technology stack.

### Drawbacks

##### This solution has a number of drawbacks:

- Developers must deal with the additional complexity of creating a distributed system.
- Developer tools/IDEs are oriented on building monolithic applications and don’t provide explicit support for developing distributed applications.
- Testing is more difficult
- Developers must implement the inter-service communication mechanism.
- Implementing use cases that span multiple services without using distributed transactions is difficult
- Implementing use cases that span multiple services requires careful coordination between the teams
- Deployment complexity. In production, there is also the operational complexity of deploying and managing a system comprised of many different service types.
- Increased memory consumption. The microservice architecture replaces N monolithic application instances with NxM services instances. If each service runs in its own JVM (or equivalent), which is usually necessary to isolate the instances, then there is the overhead of M times as many JVM runtimes. Moreover, if each service runs on its own VM (e.g. EC2 instance), as is the case at Netflix, the overhead is even higher.

### Issues
There are many issues that you must address.

### When to use the Microservice architecture?
One challenge with using this approach is deciding when it makes sense to use it. When developing the first version of an application, you often do not have the problems that this approach solves. Moreover, using an elaborate, distributed architecture will slow down development. This can be a major problem for startups whose biggest challenge is often how to rapidly evolve the business model and accompanying application. Using Y-axis splits might make it much more difficult to iterate rapidly. Later on, however, when the challenge is how to scale and you need to use functional decomposition, the tangled dependencies might make it difficult to decompose your monolithic application into a set of services.

### Guiding Principles

Key Governing Principles of Microservices Architecture
No.

| Principle Name | Statement  | Rationale | Implications|
|---------------|------------|-----------|-------------|
| 1 | Independent Services | A microservice is independent of all other services. | Independence of services enables rapid service development and deployment, and permits scalability through instantiation of parallel, independent services. This characteristic also provides resilience; a microservice is allowed to fail and its responsibilities are taken over by parallel instantiations (of the same microservice), which do not depend on other services. When a microservice fails, it does not bring down other services.| Both design and runtime independence of services are required. It is necessary for the business to determine whether providing scalability and resilience of the business function are paramount considerations. If so, MSA provides a means of achieving these characteristics.|
| 2 | Single Responsibility | A microservice focuses on one task only and on doing it well. A microservice focuses on delivering a small specific business capability. | This develops ideas including the principle of Single responsibility, Open-closed, Liskov substitution, Interface segregation, and Dependency inversion (SOLID), which is a tenet of Object-Oriented Design (OOD) and the core business domain and bounded context of DDD. Microservices are aligned to atomic business functions, which can be modified and deployed independently. To achieve this it is critical that each microservice caters to a single functional responsibility. This requires business function decomposition into atomic functional services and data exchanges.|
| 3 | Self-Containment | A microservice is a self-contained, independent deployable unit. | In order for a microservice to be independent, it needs to include all necessary building blocks for its operation, or there will be dependencies to external systems and services. This has architecture, design, implementation, and deployment implications.|


Simple Example showing independent Database for each business function, queries and interactions between each function are handled by APIs.

![Database Structure](/static/images/DDMPatterns2.png)

### How to decompose the application into services?
Another challenge is deciding how to partition the system into microservices. This is very much an art, but there are a number of strategies that can help:

- [Decompose by business capability](./Decomposing-by-Business-Domains.md) and define services corresponding to business capabilities.
- [Decompose by domain-driven](./Decomposing-by-Sub-Domains.md) design subdomain.
- Decompose by verb or use case and define services that are responsible for particular actions. e.g. a Shipping Service that’s responsible for shipping complete orders.
- Decompose by by nouns or resources by defining a service that is responsible for all operations on entities/resources of a given type. e.g. an Account Service that is responsible for managing user accounts.
- Ideally, each service should have only a small set of responsibilities. (Uncle) Bob Martin talks about designing classes using the Single Responsibility Principle (SRP). The SRP defines a responsibility of a class as a reason to change, and states that a class should only have one reason to change. It make sense to apply the SRP to service design as well.

Another analogy that helps with service design is the design of Unix utilities. Unix provides a large number of utilities such as grep, cat and find. Each utility does exactly one thing, often exceptionally well, and can be combined with other utilities using a shell script to perform complex tasks.

### How to maintain data consistency?
In order to ensure loose coupling, each service has its own database. Maintaining data consistency between services is a challenge because 2 phase-commit/distributed transactions is not an option for many applications. An application must instead use the Saga pattern. A service publishes an event when its data changes. Other services consume that event and update their data. There are several ways of reliably updating data and publishing events including Event Sourcing and Transaction Log Tailing.

### How to implement queries?
Another challenge is implementing queries that need to retrieve data owned by multiple services.

The API Composition and Command Query Responsibility Segregation (CQRS) patterns.

Related patterns
There are many patterns related to the microservices pattern. The Monolithic architecture is an alternative to the microservice architecture. The other patterns address issues that you will encounter when applying the microservice architecture.

![Microservice Pattern](/static/images/mspattern.jpg)

- Decomposition patterns
  - Decompose by business capability
  - Decompose by subdomain

- The Database per Service pattern describes how each service has its own database in order to ensure loose coupling.
- The API Gateway pattern defines how clients access the services in a microservice architecture.
- The Client-side Discovery and Server-side Discovery patterns are used to route requests for a client to an available service instance in a microservice architecture.
- The Messaging and Remote Procedure Invocation patterns are two different ways that services can communicate.
- The Single Service per Host and Multiple Services per Host patterns are two different deployment strategies.
- Cross-cutting concerns patterns: Microservice chassis pattern and Externalized configuration
- Testing patterns: Service Component Test and Service Integration Contract Test
- Circuit Breaker
- Access Token

#### Observability patterns:
- Log aggregation
- Application metrics
- Audit logging
- Distributed tracing
- Exception tracking
- Health check API
- Log deployments and changes

#### UI patterns:
- Server-side page fragment composition
- Client-side UI composition

### Known uses
Most large scale web sites including Netflix, Amazon and eBay have evolved from a monolithic architecture to a microservice architecture.

Netflix, which is a very popular video streaming service that’s responsible for up to 30% of Internet traffic, has a large scale, service-oriented architecture. They handle over a billion calls per day to their video streaming API from over 800 different kinds of devices. Each API call fans out to an average of six calls to backend services.

Amazon.com originally had a two-tier architecture. In order to scale they migrated to a service-oriented architecture consisting of hundreds of backend services. Several applications call these services including the applications that implement the Amazon.com website and the web service API. The Amazon.com website application calls 100-150 services to get the data that used to build a web page.

The auction site ebay.com also evolved from a monolithic architecture to a service-oriented architecture. The application tier consists of multiple independent applications. Each application implements the business logic for a specific function area such as buying or selling. Each application uses X-axis splits and some applications such as search use Z-axis splits. Ebay.com also applies a combination of X-, Y- and Z-style scaling to the database tier.

### Reference

- [Microservice Pattern](./Microservice-Pattern.md)

- [Decompose by Business Domain](https://github.dxc.com/ArchitectureOffice/Standards/blob/master/content/Microservices/Decomposing-by-Business-Domains.md)

- [Decompose by Sub Domain](https://github.dxc.com/ArchitectureOffice/Standards/blob/master/content/Microservices/Decomposing-by-Sub-Domains.md)

###### Information - This is a direct copy from [microservices.io](http://microservices.io/patterns/microservices.html) - This description was found to be very well aligned with DXC thinking.

