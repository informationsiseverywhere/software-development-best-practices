![Architecture Office](/static/images/dxclogo.png)

## DXC Offering Scope

### Purpose

DXC creates and develops Offerings which it takes to market, these offerings are targeted to meet repeatable market need and there by improve DXC's success in winning business with our clients. Clients are looking for DXC to provide Solutions to their Business problems. The offerings are the building blocks DXC can use to quickly develop a solution for the client reducing the need for heavy customisation or bespoke elements which are costly and create downstream problems with management and continuous improvement.

This document aims to outline the principles in which an offering should be designed and operated, to ensure that it can function and operate independently, it can scale without limitation and it does create heavy dependencies.

### Technology Characteristics

These characteristics strongly align to DXC's Design Principles - which can be found here --> [LINK](../Design_Principles/DXC-Design-Principles.md) The (#'s') will reflect the association of the characteristics to the Design Principle.

- Modular, the system should be made up of modular components, so that they can be updated, replaced or removed without impacting the entire system. [1,2,13](../Design_Principles/DXC-Design-Principles.md)

- Organisation around business capabilities instead of around technology. [2](../Design_Principles/DXC-Design-Principles.md)

- Smart endpoints and dumb pipes, explicitly avoiding the use of an Enterprise Service Bus (ESB). The move to API enabled architecture, utilising Gateways where appropriate to support Data Profiling, Security and Channeling. [3,13,20](../Design_Principles/DXC-Design-Principles.md)

- Decentralised data management with one database for each service instead of one database for a whole company. Shared databases are one of the biggest cause of heavy dependency between applications. All data requests should come through the API layers. [2,3](../Design_Principles/DXC-Design-Principles.md)

- Infrastructure automation with continuous delivery being mandatory. [5,19](../Design_Principles/DXC-Design-Principles.md)


### DXC Offering Boundaries

A key aspect of offering scope is to help defined the boundaries, DXC has structured its organisation into 9 Offering Families, these families have a Offering organisation and P&L.

  - 1: [Analytics](https://www.dxc.technology/analytics)
  - 2: [Applications](https://www.dxc.technology/application_services)
  - 3: [Business Process services](https://www.dxc.technology/business_process_services)
  - 4: [Cloud](https://www.dxc.technology/cloud)
  - 5: [Consulting](https://www.dxc.technology/consulting)
  - 6: [Enterprise and Cloud Applications](https://www.dxc.technology/enterprise_and_cloud_apps)
  - 7: [Security](https://www.dxc.technology/security)
  - 8: [Workplace & Mobility](https://www.dxc.technology/workplace_and_mobility)
  - 9: <b>Industries</b>
    - 9a - [Healthcare](https://www.dxc.technology/providers)
    - 9b - [Insurance](https://www.dxc.technology/insurance)
    - 9c - [Banking](https://www.dxc.technology/banking)
    - 9d - [Travel and Transportation](https://www.dxc.technology/travel_and_transportation)

The diagram below is a layered cake representation of DXC's portfolio, although it does call out Offering Families individually, you can quickly align the offerings orgs to the elements in the diagram.  For example Managed Public Services --> Aligns to [Cloud](https://www.dxc.technology/cloud) offering portfolio.

![DXC Portfolio](/static/images/dxclayers.png)

#### So what's the Problem?

So why do we need to outline an offering scope, with DXC's portfolio of offerings has developed over many years, and it is important to realise this as it helps to understand why previous decisions and designs have been made.

##### Primary and Dependent Offerings / Functions
We will use some terms like <b>Primary Offering</b> or <b>Function</b>, this relates to the primary purpose of the offering. i.e. a Storage Offerings primary purpose will be to provide Storage Services, or a Healthcare Application Primary purpose is to serve Health Applications/ Data.

<b>Dependencies</b> For the Primary Offering to function it is expected that it will have defined dependencies that support the operation of the offering. for example,

  - 1:Storage offering will typically require Management Services like ITSM, or Security Services.
  - 2:Healthcare Application - Hosting services, Cloud or Private, Service Management, Security Services.

We can consider compass direction (North, South, East and West) when we talk about dependencies, the image layer cake gives a good representation of this. North and South bound dependencies being represented by the stacking of Applications on Platforms, and Platforms hosted on Infrastructure, consider this as North to South. As DXC is complete Systems Integrator and Service Provider, we also provide the Consultative / Professional services aspect, which starts with Advisory and Transformation Services leading into Managed Services. This can be thought of as East to West travel.

![DXC Portfolio](/static/images/nsew.png)

What has transpired is as Offering groups looked to build-out their offerings, they started to build in Dependencies functionality that should be provided by other offerings groups as the primary owner. An example would be Security Services, which should be taken and integrated with the primary offering. Due to timing and availability of supporting services, offering groups were forced to create their own answers to supporting services to meet the expectations of offering launch priorities.

#### Turning the clock forward

So with the problem outlined above, and DXC's need to move to integratable / modularised offerings to support clients solutions. we need to ensure that offerings as they continuously develop will need to consider how they focus on the primary function of their offering and then take / integrate / utilise offerings or services from the other groups.

Example: [Platform DXC](https://github.dxc.com/pages/Platform-dxc/docs/)

#### Partner Services

????

#### What to do next

Each offering organisation should look outline what its primary offering is, and map out the dependencies it relies on
  - Security
  - Service management / Orchestration
  - Cloud Infrastructure Platforms
  - etc etc.

It should identify where DXC has existing capability from other Offering orgs, or centralised developments (e.g. Platform DXC). The Product Owners should then consider the appropriate time in the roadmap and investment to integrate / adjust the offering (using DXC Design Principles) to utilise the secondary services.

It is useful to create a dependency map, the link below, is an example of a DXC template used to outline the mapping.

[Integration NSEW](./README.md)

DXC's TDC / IRB process for investment will review and confirm the adjustment to the roadmaps.

##### What is we need to decompose the current offering.

It is likely that some offerings will need to rearchitect and decouple some services from the primary offering. This should be done as part of an agreed development/integration roadmap with the dependent offerings or services. The link below provides the guidelines to consider for the decomposing of an existing offering.

[Decomposing into SubDomains](../Microservices/Decomposing%20by%20Sub%20Domains.md)

#### Benefits to DXC and clients

- Focused investment on Primary services
- UpSell and CrossSell of other dimensions of DXC Portfolio
- Independent improvement of Individual Offerings, without huge dependencies

## Supporting Standards / Guidelines Links.

- [DXC DevOps](../DevOps)

- [DXC API Guidelines](https://github.dxc.com/ArchitectureOffice/api-design-guidelines/blob/master/Guidelines.md)

- [Microservices & Decomposing the Monolith](../Microservices/)

- [DXC Mobile Applications Development Guidelines](../Mobile/DXC%20mobile%20apps%20standards.md)

- [DXC Inner Source Guidelines](../Inner_Source)

- [DXC UI Guidelines](https://dxcportal.sharepoint.com/:w:/r/sites/uiTechnologyDualDesign/UIdesign/_layouts/15/WopiFrame.aspx?sourcedoc=%7B24FFD5AD-C11E-49E0-9BB9-5F9119644A6E%7D&file=OC_Visual_Interface.docx&action=default&IsList=1&ListId=%7B95C94414-A09F-4F35-9CD9-DA4E7900012D%7D&ListItemId=11) (This is a SharePoint site, access may need to be provided by [Karen Russell](mailto:krusse24@csc.com))

- [DXC Brand Central](https://my.dxc.com/brand-guide.html)

- [DXC Opensource Program](https://dxcportal.sharepoint.com/sites/opensource/SitePages/Home.aspx)

- [DXC High Performing Operations](../High_Performing_Operations)
