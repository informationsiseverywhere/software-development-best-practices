### Offering Scope FAQ

### Purpose

The FAQ is created to support the review teams as they look at offerings determine the scope of the primary offering, its dependency on other offerings and it overlap with other primary offerings and services DXC provides.  Each questions supports a set of criteria needed to be able assess the maturity of the offering as we look to move toward better Integrateable, right-sized offerings.

### FAQs

- <b>1:</b> Does the Offering / SOW Description clearly define what is in scope and not in scope?  1a) Identify/List what (features, capabilities, offerings and state which) are in scope (standard) and what is optional.  (1b) Identify to which Offering Family each of these correspond? (1c) Which of these are reliant on/directly from external Partners. (1d) Identify your source (documents and page) for this determination.  

<b>Why?</b> this question helps to understand if the in-scope elements of the offering are clearly defined, also know what is not included helps to understand which might be common dependencies which will support question 3:

- <b>2:</b> Does the In-scope characteristics of the offering sit within the primary offering family.  (this has been superseded by combining into question 1 (1b), above.)

  - [Analytics](https://www.dxc.technology/analytics)
  - [Applications](https://www.dxc.technology/application_services)
  - [Business Process services](https://www.dxc.technology/business_process_services)
  - [Cloud](https://www.dxc.technology/cloud)
  - [Consulting](https://www.dxc.technology/consulting)
  - [Enterprise and Cloud Applications](https://www.dxc.technology/enterprise_and_cloud_apps)
  - [Security](https://www.dxc.technology/security)
  - [Workplace & Mobility](https://www.dxc.technology/workplace_and_mobility)
  - <b>Industries</b>
    - [Healthcare](https://www.dxc.technology/providers)
    - [Insurance](https://www.dxc.technology/insurance)
    - [Banking](https://www.dxc.technology/banking)
    - [Travel and Transportation](https://www.dxc.technology/travel_and_transportation)

<b>Why?</b> Offerings should try not to provide services or capability which can be provided by other offering organisations.

- <b>3:</b> Are the North, South, East and West bound dependencies [Offering Scope Doc](./DXC Offering Scope.md) identified. (Unclear how this question adds additional information beyond what can be determined by answering the broader question 1 above)

<b>Why?</b> this question looks to understand what are the typical dependencies this offering has, and have they been clearly identified.

- <b>4:</b> Does the Offering identify how other offerings should integrate with it? (APIs, Protocols)  (For this question to be actionable, would this need to request the list of  API's that are documented in the design, and those the reviewers think are inadequate and/or missing?)

<b>Why?</b> this question helps to ascertain if the offering team have defined the integration mechanism for the primary offering.

- <b>5:</b> Does the Offering (managed offerings only) require and callout any of the following (Should this list include other specifics for example, Bionix, PDXC, APA, as well generic Automation with specific explanation of how the Automation is being used.)

  - ITSM - Service management
  - Automation
    - Deployment
    - Configuration
    - Run and Maintain
  - Security
    - Identity Access management
    - Log Management

<b>Why?</b>  Platform DXC is DXC's delivery platform, all managed services should be looking to integrate with it, and not incorporate as part of the default service the Functions and Features Platform DXC provides.
