## DXC Offering Development Standards

The following document outlines the standards DXC requires of its offerings. The standards will be validated during the various check points within DXC's PLM Process.

Typically these standards focus on the Infrastructure, Platform and Application based offerings across Horizontal and Industry.

## Design Configuration

##### Develop a loosely coupled based architecture -

Loose coupling is an approach to interconnecting the components in a system or network so that those components, also called elements, depend on each other to the least extent practicable. Coupling refers to the degree of direct knowledge that one element has of another.

The goal of a loose coupling architecture is to reduce the risk that a change made within one element will create unanticipated changes within other elements. Limiting interconnections can help isolate problems when things go wrong and simplify testing, maintenance and troubleshooting procedures.

A loosely coupled system can be easily broken down into definable elements. The extent of coupling in a system can be measured by mapping the maximum number of element changes that can occur without adverse effects. Examples of such changes include adding elements, removing elements, renaming elements, reconfiguring elements, modifying internal element characteristics and rearranging the way in which elements are interconnected.

Although loose coupling minimizes unnecessary interaction among system elements, it can create problems when such interaction is desired. For example, in some data-centric systems a high degree of element interdependence is necessary for synchronization in real time.

##### API Enabled and Defined -

Open Based APIs -

Private APIs -  should follow the [DXC API Design Guidelines](https://github.dxc.com/ArchitectureOffice/api-design-guidelines/blob/master/Guidelines.md).

Homegrown or Vendor -

API Gateways -

##### Design Thinking -

Blah blah blah blah blah

## Integration

##### Integration Matrix / Testing - Your offering should validate and test its operation against

	a: North and South bound offerings / Services - Hosting, Application Support etc

	b: East and West bound offerings - Advisory and or Transformation services.

![IMAGE](/static/images/intpic.png)

##### DXC Standard Integration Requirements

- Platform DXC (ADD LINK)
- Security

## Common Features and Functions.

##### Defined Automation of Deployment, Configuration and Maintain via approved scripting tools -

##### Log Exposure, ensure you provide clarity on the Logs to be tracked, and key events in which triggers should be activated

## Build Operations

##### Develop the Continuous Integration and Delivery,

Design to enable the development of new features / functions and fixes to be deployed autonomously

##### Centralise development work into Collaborative space

Use of GitHub will be positively encouraged, although not mandated as much of the artefacts as possible should be developed as markdown documents.

##### Other considerations

* Consider Continuous Integration and Delivery, design to enable the development of new features / functions and fixes to be deployed autonomously

* Identifies closely bound and loosely coupled component groups to define critical Internal Offering Interfaces

* Identifies the Configuration Items associated with components, constants and variables within those configurations

* Identifies security controls and how they effect differing levels of security assertions

* Identifies the Infrastructure quality and resource needs for each component

* Identifies common components with other offerings

* Identifies core software products and any vendor reference architectures or patterns exploited

* Identifies key customer proto personas and customer journeys associated with the offering

* Identifies key value creators for the offering

* Identifies clear feature plans for the offering

* Identifies clear QoS plans for the offering

* Identifies underpinning domain and information model for the offering

* Offering health and performance management components
