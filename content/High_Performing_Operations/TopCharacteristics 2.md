High performing incident operations...

### Have no tolerance for repetitive manual break-fixes
Anything occurring 3+ times in a rolling 6 month period is cause for serious concern; less than 5% of incidents at any time should be in this category and those should be actively under problem investigation

### Quickly and accurately route work
Issues find themselves in front of someone that can action it fast and with a few hops as possible-—ideally 0 human hops

### Ensure that all human-involved activities add value
When humans are involved there must be value-add activity occurring (e.g., on a particular class of issue if the ‘resolution’ is consistently ‘no action required’ then the whole class of incidents should be reviewed for possible auto-suppression and/or improved monitoring)

### Ensure incident responses are timely
Related to the above is that response to an incident is consistently in a timeframe that matters.
(e.g. if teams don’t respond to a ‘high cpu’ or ‘network down’ incident in 5 minutes or less then there isn’t much point in doing so as, if it’s really an issue many other things will break before teams arrive on scene.)

### Always address the root cause
When fixing an issue, the root cause of the issue is fixed not just the symptom. If it can’t be immediately fixed it is at least known and documented as a known risk until it can be fixed (e.g. just deleting excess files off a disk doesn’t ‘resolve’ a disk full issue, fixing it means changing the practice that caused the disk to fill up)

### Maintain clear ownership and accountability
Issues are always owned end-to-end by both a named operations lead and by extension all teams that touch the issue. There is zero room for a “that’s not our issue” mentality (this is especially true between infrastructure and apps issues… it’s not ‘just’ an inf or 'just' an app issue but an issue for both and both teams need to be constantly working together to solve issues. This is at the core of modern operations aka DevOps.)

### Focus on work elimination before work automation
Automation should only be applied to what can’t be easily eliminated. Spending money and time to automate things that should have just been eliminated entirely via addressing root causes is an easy automation for automation's sake trap that can lead unstable environments (e.g., an environment full of automatic duct-tape appliers vs. an environment that is truly robust and stable)

### Apply fixes horizontally
Where a fix is applied to a repetitive set of issues, that fix is immediately considered for broader rollout
(e.g., a common fix is ensuring that all logs on a server are set to auto-rollover; log files should never fill up a disk and yet this is still a common issue)

### Reframe blockers as opportunities
High performing operational teams have a clear pipeline for inverting operational blockers into opportunities (e.g. "We can't make these incidents go away because a 3rd party vendor's app is causing problems in the environment" can be reframed as "By collecting extensive data on the nature of instability in the environment, we will make an aggressive play to grow the account and take over management of a 3rd party vendor's operation that the data shows is currently inefficient."

### Utilize action oriented reporting
In any operations reporting is always structured around value achieved vs. the value opportunity. Just simply reporting deltas (e.g., "we had 10% reduction in incidents") makes for good PowerPoint pages but isn't action oriented--it's just movement against an unknown goal or objective. Reporting a 10% reduction in incidents against a target of 60% (based on an initial opportunity assessment of completing a set of defined actions) is far more realistic and action oriented. Metrics should drive future behavior not simply report previous action.

### Always know where they stand
Top performing groups never say they are top performing. They celebrate their successes, but are also cognizant and open about their challenges.
