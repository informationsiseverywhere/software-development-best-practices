# High Performing Operations

## Characteristics of High Performing Incident Ops
Within global Bionix efforts the OE&amp;E data science team is often asked for "best practices" around high performing incident operations based on what we see in analyzing data across hundreds of accounts and operations. 

This repo contains a high-level summary of the key characteristic of high performing groups based on observations in rolling out Bionix. While not completely comprehensive, this provides a good starting guide for any incident operation to self-assess performance by asking "Do we completely meet all these characteristics?"

If you have changes or additional characteristics, please submit a pull request!

We will continue to develop and evolve these for other operational areas.

[High Performing Operations](https://github.dxc.com/ArchitectureOffice/Standards/blob/master/content/High_Performing_Operations/TopCharacteristics%202.md)  
