# Non Functional Requirements

##  Functional and Non-Functional Guidelines to Application types in Business.

### Purpose

This document is aimed to provide guidance to Designers / Architects / Engineers and Developers on the Functional and Non-Functional criteria for certain types of applications.

### What are Non-Functional Requirements

Non-functional Requirements capture conditions that do not directly relate to the behaviour or functionality of the solution, but rather describe environmental conditions under which the solution must remain effective or qualities that the systems must have. They are also known as quality or supplementary requirements. These can include requirements related to capacity, speed, security, availability and the information architecture and presentation of the user interface.

### Functional Application Descriptions

To start we will outline the typical application types we see in the enterprise, below are the considered groupings of applications which we will then outline the functional and non-functional criteria for:

- <b>1: Business / Mission  Critical applications</b> - These are the primary business applications which if impacted would stop the business from operating. (Patient Record System, Finance Brokering Applications, Purchasing System, Asset Management System).
- <b>2: Business Support applications</b> - These application provide the business with day by operations, although important if there operation was impacted the business would continue to provide business to its customers (HR, Workplace, Sharepoint, O365).
- <b>3: Developer Applications</b> - Within the majority of businesses there is the need to have development environments, these support the developer community i their tasks. These environments need to operate and be supported differently. (Sandpits, UAT)
- <b>4: Operational Applications</b> - Within any business there is the need for Operational / Support systems. In some cases these could be tied closely to business critical applications (IDAM service - if i can't login, i cannot use the app). Other Operational Apps might be less impacting, but important to the business overall operation (Service Management, Back up Systems, Security Apps)

The examples given above may vary based on the aspects of business, a simple example may find that a software development company could consider their dev environment critical to the business.  These may also be subjective based on the stakeholder that is representing the opinion, so interpreting these objectively should be reflected on.

### Cost vs Risk

The decision around how to align the Non-Functional Requirements is a Cost vs Risk decision, creating a solution to support the Mission Critical requirements is costly as you have reinforce many aspects of the solution adding cost. Applying this to less demanding systems means you stack unnecessary costs and this impacts its suitability to the client or approver.

![Cost vs Risk](/static/images/costrisk.png)  


### Functional Table

Functional Criteria will be more specific for each application, therefore although we can categories the applications, there would be too many variables to consider and try to capture in a consolidated set of principles.

| Functional   | Description  | 1:Business Critical Applications |  2:Business Support Applications | 3:Developer Applications | 4:Operational Applications |
| -------| -----------|----------|------|-----|-----|
| Business Need  | What is the Business Purpose of the application  | Defined Per Application | Defined Per Application | Defined Per Application | Defined Per Application |
| Integration Need  | What are the key integration aspect to support the Business and Support operation.  | Defined Per Application | Defined Per Application | Defined Per Application | Defined Per Application |
| Test Plan   | What are the acceptable test criteria.  | Defined Per Application | Defined Per Application | Defined Per Application | Defined Per Application |
| Use Cases  | Functional requirements are typically represented as a series of steps. The usecases puts a collection of functional requirements into the context of user action, which typically eliminates a lot of ambiguity that makes it’s way into an out-of context list of system shalls. | Defined Per Application | Defined Per Application | Defined Per Application | Defined Per Application |
| User Stories / Needs | "functional requirements are typically captured in the following syntax: “As a {user}, I can {do something} so that {I receive some benefit}. When used appropriately, the user story syntax is brilliant at capturing user goals, functional requirements, and business benefits altogether in one concise statement.  " | Defined Per Application | Defined Per Application | Defined Per Application | Defined Per Application |

### Non-Functional Table

The non-functional criteria can be easier to capture some consolidated principles based on the application type. The purpose of this table is not to show the specifics values for a business, but to show the difference between the four application types and how we should be considering the deltas, as one size does not fit all.

| Non-Functional  | Description  | 1:Business Critical Applications |  2:Business Support Applications | 3:Developer Applications | 4:Operational Applications |
| -------| -----------|----------|------|-----|-----|
| Security | Login / Access levels  | Restricted based on user profile, Invited / Approved Access.  | Restricted based on user profile. Invited / Approved Access. | Consider Open Source / Inner Source, user login, but initial open vs closed based access.| Restricted based on user profile. Invited / Approved Access. |
| Security | Access permissions for application data may only be changed by the system’s data administrator  | Yes  | Yes   | Access to data will be under the control of the developers  | Yes   |
| Security | Password requirements – length, special characters, expiry, recycling policies, 2FA"  | Yes  | Yes  | Functional security, not restrictive   | Yes   |
| Security | Inactivity timeouts   – durations, actions, traceability" |  Yes  | Yes  | Inactivity timeouts will be at the control of the developers   | Yes (Timing variants maybe required from other Applications)   |
| Security | System data backed up every x hours and copies stored in a secure off-site location  | Meeting RTO/RPO SLA    | Meeting RTO/RPO SLA    | Developers control Data Backup (e.g. Automated Snapshots) | Operations Provider SLAs, Service Levels may vary from other applications |
| Security | Encryption (data in flight and at rest) – All external communications between the system’s data server and clients must be encrypted | Inline with Security Requirements (Reg / Data Classification) | Inline with Security Requirements (Reg / Data Classification)    |Optional, Encryption at the discretion of the developers | Inline with Client / Operational requirements, Consider Key Code Management services as well   |
| Security | Data Classification / System Accreditation: All Data must be protectively marked and stored / protected. | Inline with Security Requirements (Reg / Data Classification)    | Inline with Security Requirements (Reg / Data Classification)  | Optional, Encryption at the discretion of the developers   |  Inline with Security Requirements (Reg / Data Classification) |
| Security | Systems network Access, availability (Internal / External) | Defined by Customer needs  | Defined by Customer needs  | Defined by Developer needs   | Defined by Operation needs   |
| Security | Regulatory compliance of Operation | Defined by Customer needs  | Defined by Customer needs    | Defined by Developer needs   | Defined by Operation needs   |
| Audit | System must maintain full traceability of transactions  | Yes   | Yes  | Discretion of the developers   | Yes  |
| Audit | Audited Objects are defined   | Yes    | Yes    | Discretion of the developers   | Yes   |
| Audit | Audited database fields – which data fields require audit info? | Yes, meeting regulatory requirements e.g. GDPR     | Yes, meeting regulatory requirements e.g. GDP    | Discretion of the developers   | Yes, meeting regulatory requirements e.g. GDPR   |
| Audit	| File characteristics   – size before, size after, structure"   | Yes if required by audit compliance    | Yes if required by audit compliance    | Discretion of the developers   | Yes if required by audit compliance   |
| Audit	| User and transactional time stamps, etc"    | Yes  | Yes  | Updates tracked through version control systems  | Yes  |
| Capacity  | Throughput – how many   transactions at peak time does the system need to be able to handle  | Sized Common Trans Volume + 20% Buffer (Design should allow burst capacity as required)    | Sized Common Trans Volume + 10% Buffer (Design should allow burst capacity as required)    | Not Necessary, Although final Load tests might require full scale testing | Sized Common Trans Volume + 5% Buffer (Design should allow burst capacity as required)   |
| Capacity  | Storage – (memory/disk) –  volume of data the system will page /  persist at run time to disk | Sized based on response time of transactions, and transactional loss risk of system failure and transactions not committed to Disk | Sized based on response time of transactions, and transactional loss risk of system failure and transactions not committed to Disk | Discretion of Developers   | Non-Specific / Defaults to meet operating requirements of systems   |
| Capacity	| Year-on-year growth   requirements  (users, processing & storage) | Planned aligned with Business Growth | Planned aligned with Business Growth    | Discretion of Developers   | Planned aligned with operational growth   |
| Automation 	| Execution of Deployment   | Yes    | Yes    | Yes   | Yes  (inc deployment Engine) |
| Automation 	| Execution of Configuration  | Yes    | Yes    | Yes   | Yes  |
| Continuous 	| Delivery of rolling updates and changes | Yes, Certain systems may have higher verification change processes needed before upgrades are rolled out    |Yes    | Yes, developer requires functionality to roll-back and forward at there discretion   | Yes   |
| Control  | Identify who controls changes and updates to the environment | Yes, Change Control Board - Business | Yes, Change Control Board - Business | Developer Controls   | Yes, Change Control Board Operations |
| Performance	| Response times – application loading, browser refresh times, etc."  | Defined by business requirements | Defined by business requirement| Developer Discretion   | Defined by operational requirements   |
| Performance	| Processing times – functions,   calculations, imports, exports"  | Defined by business requirements  | Defined by business requirement | Developer Discretion   | Defined by operational requirements   |
| Performance	| Query and Reporting times – initial loads and subsequent loads, ETL times"  | Defined by business requirements | Defined by business requirement  | Developer Discretion   | Defined by operational requirements   |
| Performance | Interoperability  | Defined by business requirements   | Defined by business requirement | Developer Discretion | Defined by operational requirements |
| Availability  | Hours of operation  | Defined by business requirements  | Defined by business requirement | Developer Discretion | Defined by operational requirements |
| Availability	| holidays, maintenance times, etc" | Defined by business requirements     | Defined by business requirement    | Developer Discretion   | Defined by operational requirements   |
| Availability	| Locations of operation – where should it be available from, what are the connection requirements? | Defined by business requirements     | Defined by business requirement    | Developer Discretion   | Defined by operational requirements   |
| Reliability  | The ability of a system to perform its required functions under stated conditions for a specific period of time.  | Defined by business requirements     | Defined by business requirement    | Developer Discretion   | Defined by operational requirements   |
| Reliability  | Mean Time Between Failures – What is the acceptable threshold for down-time?  | Defined by business requirements     | Defined by business requirement    | Developer Discretion   | Defined by operational requirements   |
| Reliability	 | Mean Time To Recovery – if broken, how much time is available to get the system back up again?"   | Defined by business requirements     | Defined by business requirement    | Developer Discretion   | Defined by operational requirements   |
| Recoverability  | Recovery process | Image rebuild of Application, Restore or Regeneration of Data  | Image rebuild of Application, Restore or Regeneration of Data  | Developer Deploys new Environment   | Image rebuild of Application, Restore or Regeneration of Data |
| Recoverability  | Recovery Point Objectives (RPO) | Aligned to Business Tolerances/SLAs e.g NHS Patient Data = Zero Data Loss| Typically lower than Critical Business Applications  | Discretion of the developers (Self Service) | Typically lower than Critical Business Applications, although certain systems may have a higher RPO e.g. IDAM  |
| Recoverability  | Recovery Time Objectives (RTO)  | Aligned to Business Tolerance/SLAs  | Typically lower than Critical Business Applications  | Discretion of the developers (Self Service)   | Typically lower than Critical Business Applications, although certain systems may have a higher RTO e.g. IDAM  
| Recoverability| Backup frequencies – how often is the transaction data, config data, code backed-up?"   | Defined by business requirements     | Defined by business requirement  | Developer Discretion   | Defined by operational requirements   |
| Robustness | The ability of the system to resist change without adapting its initial stable configuration – operational characteristics with growth? | Yes    | Yes    | Not applicable   | Yes   |
| Robustness | Fault trapping (I/O) , Application Hooks, SMNP – how to handle failures ?"   | Yes    | Yes    | Discretion of the developers   | Yes   |
| Integrity | Application Integrity  | Yes    | Yes    | Not applicable   | Yes   |
| Integrity | Data integrity – referential integrity in database tables and interfaces | Yes    | Yes    | Discretion of the developers   | Yes   |
| Integrity | Information Integrity – during transformation  | Yes    | Yes    | Discretion of the developers   | Yes   |
| Maintainability | Conformance to Enterprise Architecture standards  | Yes    | Yes    |  Aware   | Yes   |
| Maintainability | Conformance to Technical design standards    | Yes    | Yes    | Aware   | Yes   |
| Maintainability | Conformance to coding standards   | Yes    | Yes    |  Aware   | Yes   |
| Maintainability | Conformance to best practices.    | Yes    | Yes    |  Aware   | Yes   |
| Maintainability | Patch Management    |   Yes    | Yes  |  Discretion of the developers   | Yes   |
| Usability | User Standards (Look / Feel)  | Yes    | Yes    | Discretion of the developers  | Yes   |
| Usability	| Internationalisation / localisation requirements – languages, spellings, keyboards, etc" | Yes    | Yes    | Discretion of the developers   | Yes   |
| Presentation | Ensure UI compatibility with common browsers | Yes (Exceptions need to be explicit) | Yes (Exceptions need to be explicit)    | Aware  | Yes (Exceptions need to be explicit)  |
| Sustainability | Carbon Impact / PUE (Power Usage Effectiveness) | | | |
| Sustainability | Equipment Energy Star Rating | | | |
| Sustainability | Economic Development  | | | |
| Design Principles | The system should respect the [DXC Design Principles](https://github.dxc.com/pages/ArchitectureOffice/Standards/Design-Principles/DXC-Design-Principles/) | Yes    | Yes    | Yes   | Yes   |
