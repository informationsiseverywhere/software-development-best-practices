# API Standards

## API Design Starts Here

This documentation repository is intended to provide a set of guidelines for designing a robust REST API.

It is split into three major parts:

1. The (infamous) "big doc" - i.e. the [guidelines themselves](Guidelines.md).
2. A growing set of [clarification and example articles](Articles) (which are also referenced from within the guidelines).
3. [A set of proposals](https://github.dxc.com/ArchitectureOffice/Standards/issues?q=is%3Aissue+is%3Aopen+label%3Aapi-design-proposal) that may or may not eventually be included in the guidelines.

If you wish to comment on the guidelines then you are encouraged to raise [issues](../../../../issues) with appropriate tag(s).

