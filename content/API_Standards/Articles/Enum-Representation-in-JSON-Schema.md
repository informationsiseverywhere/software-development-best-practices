## ENUM Representation

Enums - i.e. properties that can be set to one of a discrete set of values - are supported in JSON (Hyper)-Schema using the following syntax:

```
{
    "$schema": "http://json-schema.org/schema#"
    "title" : "My resource OPTIONS response",
    "type": object
    "properties": {
        ...
        "gender": { "type": "string", "enum": ["male", "female"]},
        ...
        "discount_rate": { "type": "integer", "enum": [10, 20, 30, 40]},
        ...
    }
}
```

It is fairly obvious that this means that the resource representation being described here can have a `gender`
property set to a string whose value is either `male` or `female`, and a `discount_rate` property whose value
can be one of the listed integer values.

While numeric values are, for the most part, easy to deal with, for language sensitive
values (so, say `masculino` and `femenina` for Spanish gender), most SORs will actually
represent the value as a code, internally, and then translate the value for external
representation. 

The original approach to use of such coded enums was indeed that the API will expose and consume the external value, 
while the SOR performs translations to internal values based upon the `Accept-Language` and `Content-Language` headers.

This worked fairly well to begin with, but has some shortcomings that force the
consumer to be more closely coupled to the server than we would like. 

For example:

* When the value choices should be presented to the consumer's own clients (e.g. a user) in the same
order no matter what language, a different "sort" is necessary for each language, or, worse still, the
consumer has to hard-code the order for each language.
* Similarly, when consumer state (e.g. visible tabs) depends on an enum property, the feature switch must
be coded for each supported language.

The general point is that the consumer
really does need to see both translated and "normalized" values in order to remain decoupled from the server.

One approach would be to have the translation in the consumer - but most of our SORs already have a
rich set of translations for their encoded enum values which we would prefer to exploit.
Additionally, this encoding is SOR-specific - Gender might be stored as `1` or `2` in one SOR while, say, `M` or `F` in another - which
would mean that the consumer would need translations for every code from every SOR with which it wants to
communicate. So would appear to be better to let the consumer know the contextual translation to show to its consumers - but
it still needs to know the code value for language-agnostic purposes.

The JSON Schema maintainers recognize this problem, and, for a future version of
JSON Schema, a number of suggestions have been proposed (see: ["Multilingual meta data", "choices (v5
proposal to enhance enum)"](https://github.com/json-schema/json-schema/wiki/multilingual-meta-data-%28v5-proposal%29). 

Unfortunately all these require an update to JSON Schema (i.e. they won't "work" - or are illegal -
with the current stable JSON Schema specification) and they don't completely solve the issues that our consumers
experience, anyway.

However, in the comments of the ["v6 annotation: named enumerations"](https://github.com/json-schema-org/json-schema-spec/issues/57)
issue, an already-compatible suggestion is proposed - use of the `oneOf` feature:

```
{
    "$schema": "http://json-schema.org/schema#"
    "title" : "My resource OPTIONS response",
    "type": object,
    "properties": {
        ...
        "gender": { "type": "string",
            "oneOf": [
                { "enum": ["a"], "title": "male"},
                { "enum": ["1"], "title": "female"}
            ]
         },
         ...
         "discount_rate": { "type": "integer", "enum": [10, 20, 30, 40]},
         ...
     }
}
```

To be clear, the `gender` specification 'says' 

> _this property can contain one of the following values_
> (followed by a list, each member of which says) _a fixed value of &lt;the value in the `enum` array&gt;, with a
> translation of &lt;the value of the `title` property&gt;_ 

which effectively describes an enum of coded values, each with their contextual translation. 

* The `title` property used here is existing JSON Schema metadata - now exploited to carry the
translation of the coded value.
* The value set can now be sorted by either code or title
* Logic linking consumer features to enumerated values can rely on the coded value.

The above example displays the "contextual" schema presented in response to the `OPTIONS` request. Noncontextual
schemas (the schemas API) would contain all translations for all possible values, presented in this
way:

```
{
    "$schema": "http://json-schema.org/schema#"
    "title" : "My resource schema",
    "id": "http://my.schemas.api/my-resource",
    "type": object,
    "properties": {
        ...
        "gender": { "type": "string", "oneOf": [
            { "enum": ["a"], "title": "male", "description": "en"},
            { "enum": ["a"], "title": "masculino", "description": "es"},
            { "enum": ["1"], "title": "female", "description": "en"},
            { "enum": ["1"], "title": "femenina", "description": "es"}
            ...
        ]},
        ...
        "discount_rate": { "type": "integer", "enum": [10, 20, 30, 40]},
        ...
    }
}
```

Note that in the schema example another piece of JSON Schema metadata is used to assist in identifying the context of the title string (here it is a simple 
language code but it can be anything that enhances the understanding of the schema). These _MAY_ also appear in the `OPTIONS` response, but are
not required. 

To be clear, __this DOES change how the actual value is conveyed in the resource representation both in responses and requests__. The values in
a representation and those passed in request bodies must now be the __coded values__ and not the __translated values__. This is the strict meaning
of the `oneOf` structure used here. If a JSON Schema parser/verifier were to be run on a representation that used the `title` values, validation would fail.

Note, also, the non-language-specific `enum` remains a simple enum - the intention is to support
BOTH types of enum. The original we call a "simple enum", the new proposal being a "decorated enum".

In summary, the effect on the server is:

* For non-translated values use simple enums
* For translated/encoded values use decorated enums
* In representations, accept and return only values expressed in the `enum` portion of each JSON Schema description - i.e. the encoded values

The effect on the consumer is as follows:

* If the JSON Schema description of a property is a simple enum, nothing changes
* If the JSON Schema description of a property is a decorated enum - i.e. it is a `oneOf` object where
every entry contains at least a one-element `enum` array and a `title` string - then 
    * expect and set representation values based on the `enum` array content (so, basically the same as now); 
    * expose values to user interfaces based upon the `title` values; 
    * perform uniform sorts and feature switches based upon the `enum` array content.

It should be fairly simple (particularly in Javascript) to produce utility code to consume
these two different structures and produce an internal "enum" object that supports both semantics
uniformly.
