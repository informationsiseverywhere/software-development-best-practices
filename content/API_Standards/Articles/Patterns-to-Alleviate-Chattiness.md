{{% notice info %}}
The content of this article is derived from a [blog post from 2015](https://dxcportal.sharepoint.com/sites/apiCommunity/SiteAssets/API%20Community%20Wiki/API%20Blogs/shutting-up-a-chatty-api.pdf)
{{% /notice %}}

## Patterns to Reduce Chattiness in an API

The basic pattern for REST API interaction as described in the [API Design Guidelines](https://github.dxc.com/pages/ArchitectureOffice/Standards/API_Standards/Guidelines/), is as follows:

* You have a link
* You invoke OPTIONS on the URI contained in the link to find out what you can do with it
* You do what you want with it (so GET, PATCH, DELETE)
* You end up somehow with another link
* Iterate with the new link

Using a simple navigation example;

* You have a link
* You invoke OPTIONS on its URI
* It tells you that you can GET it
* You GET the URI
* The representation has hypermedia in it, and you find an interesting link
* Iterate with the new link

In this pattern,

* the OPTIONS request returns
current interaction possibilities based upon the state of the target resource, its current business context, and the
security context of the request
* returned representations contain HAL link objects describing the hypermedia
* There _should also be_  a schema API that gives static information about every "type" of resource in the target API, the contents of which are
referred to by the API resource representations themselves by "type" links in their contained hypermedia.

However, taking this pattern literally as it is can produce a very "chatty" client - won which issues a lot of network requests - which can lead to performance issues.

In order to help alleviate with this chattiness, the guidelines prescribe three patterns that augment the simplistic
interaction presented above.

### The Content-Location pattern

The first is a pattern already built into the HTTP specs and one which our API producers and consumers _should_ endeavour to exploit
to the full.

It is based on using the `Content-Location` header in the response to an update request (i.e. the response to a
PATCH, POST or PUT request).

This pattern is [technically described in the main guidelines](https://github.dxc.com/pages/ArchitectureOffice/Standards/API_Standards/Guidelines/#the-content-location-response-pattern),
but is fully fleshed out and explored in [a separate article](./Update-Responses-and-CLP.md).

Consequently we will not re-address it here.

### The HAL hypermedia cache pattern

The second pattern is part of the HAL representation format standard, and its presence in that standard is
one of the reasons we chose HAL as our standard resource representation format.

The pattern is pretty simple in concept. The following example is based upon the example cited in the HAL standard.

A simple representation before the use of the hypermedia cache pattern:

```
{
    "_links": {
      "self": { "href": "/books/the-way-of-zen" },
      "author": { "href": "/people/alan-watts" }
    }
}
```

The same representation implementing the hypermedia cache pattern:

```
{
    "_links": {
        "self": { "href": "/books/the-way-of-zen" }
        "author": { "href": "/people/alan-watts" }
    },
    "_embedded": {
        "author": {
            "_links": {
                "self": { "href": "/people/alan-watts" }
            },
            "name": "Alan Watts",
            "born": "January 6, 1915",
            "died": "November 16, 1973"
        }
    }
}
```

It should be fairly clear what is happening here: the representation of a linked resource from the referring resource's
representation is itself included in the  referring resource's
representation within an object named `_embedded`, which is a reserved optional property of a HAL
representation.

So NOW the interaction pattern becomes:
* You have a URI ( /books/the-way-of-zen)
* You invoke OPTIONS on the URI
* It tells you that you can GET it
* You GET the URI
* The representation has hypermedia in it, and you find an interesting link (`author`)
* You check to see if there is an `_embedded` object in the representation (there is)
* You check to see if there is a property of that object there with the _same name as the link you are interested in_ - so, again, `author` (there is)
* You use the value of that property as the representation to work with

The result is the avoidance of one OPTIONS request and one GET request.

Embedded resources can, themselves, have embedded resources, so the server can serve up an entire resource network if so desired!

As long as the client follows the pattern of checking for an `_embedded` representation in the current representation before explicitly
GETing the linked resource, the server may or may not serve up embedded representations at will, and the client will interact correctly whether
representations are embedded or not.

#### Disadvantages of the pattern

There are some down-sides to using this pattern:

* It doesn't interact well with standard use of conditional requests (for HTTP caching and optimistic
locking) - e.g. an ETag header in the HTTP response will apply to, not only the original resource
addressed by the request, but also to the `_embedded` resources that the server has seen fit to return
and thus will not be very useful for individual resource control.
* It can encourage the client to work with resources quasi-offline, meaning that the client may not
become aware of updates to the embedded resources (e.g. from other clients) in a timely fashion.
* Embedding of collection resources with large content sets would be complex to manage.
* Client-side control of the mechanism could cause server-side issues and inconsistencies. e.g. what
if the client tries to get everything from a large root collection in a single request - or, worse,
attempting to obtain a set of unrelated resources while using the semantics of a single-resource URI.
* On the other hand server-side control of the mechanism could involve overheads and side effects
that are undesirable to the client (payloads much more cumbersome than the client actually needs -
and, of course, breaking caching algorithms).

#### Mitigating the disadvantages

Part of the problem is due to the fact the raw pattern, as described in the HAL specifications, implies that the
same URI can be used for all representations of the resource, with or without embedding.

This means that HTTP mechanisms that rely on the URI as part of the criteria for establishing the identity of a
representation will not be able to distinguish between the two cases.

For example, the `ETag` header, if it is to be used to it's full power, must contain a "strong" value - effectively a
unique value that guarantees that the bytes of one response body are exactly the same as those of another
response body if the URI and the `ETag` are the same. Obviously with a non-deterministic approach to
embedding this is not going to be the case, thus breaking mechanisms that rely on strong ETags at least. Of
course, one can use weak ETags but these are not applicable to one of the more powerful features that ETags
and conditional requests provide - optimistic locking (For more information about the semantics and use of ETags, see
the [main guidelines](https://github.dxc.com/pages/ArchitectureOffice/Standards/API_Standards/Guidelines/#enterprise-resource-types)).

The key is, then to change the URI.

Note firstly that in our approach to REST, portions of the URI before the query parameters are considered to
represent the _identity of the targeted resource_, while the query parameters are used to "constrain" (i.e. tailor)
the _representation of the resource_.

In most of these cases we are concerned with being able to distinguish between _different representations_ of the _same
resource_.

Consequently, we should use query parameters to request optional embedding, while, in the absence of query parameters,
the server should either use no embedding, or should uniformly apply the same embedding each time the resource is requested.

This would fixes ETag, conditional request, and caching issues, and will share control of the
feature between the client and the server. Additionally, if the client is specifically requesting embedding (by using a
differentiated URI), then it will be implicitly aware of the dangers of working quasi-offline - but still has effective
ETags with which to handle its expectations and refreshing needs.

One approach to allow the client to request certain embedding semantics, is to have the server publish a set of "views"
that it provides for resources. This is easily catered for in the GET interaction descriptor(s) of the OPTIONS response
pertaining to the resource, allowing for query parameter whose value can be a expressed as a enum in JSON Schema
syntax, the values of which are the names of the available views.

Views may even be parameterizable, using additional query parameters, although the availability such facilities
would be better expressed using multiple GET interaction descriptors in the OPTIONS response - one for each view -
in order to highlight the relationship between a particular view and the query parameters pertaining to its parameterization.

As with most APIs, each service engine is free to apply its own implementation approach so long as the above style of exposing
the functionality is uniform across implementations.

#### When to embed

{{% notice info %}}
to be completed
{{ /notice }}

#### Avoiding the OPTIONS request

The hypermedia cache pattern is good at eliminating resource retrievals, but when it comes to
modification of resources, each embedded resource will still require an OPTIONS request to find out what can be
done and how to do it, and these OPTIONS requests are a major source of chattiness in an API.

The guidelines explain that OPTIONS is the "standard" method to use to obtain interaction data pertaining to a
resource. Here, standard refers to the allusion of such a use in the HTTP specs for the OPTION method, the fact that many APIs do use this approach,
and the fact that implementations of frameworks such a JAX-RS can provide default OPTIONS support that includes response body content
describing the interaction and structural features of the addressed resource.

Some hypermedia representation formats (e.g. Siren) embed interaction data within the resource
representation, which avoids the use of an OPTIONS request to obtain such information, but arbitrarily
ties the interaction options with the resource state information that a representation is really meant to convey

One of the FAQs relating to HAL explains why this isn't so with HAL

> Omitting &lt;interaction metadata&gt; from HAL was an intentional design decision that was
> made to keep it focused on linking for APIs. HAL is therefore a good candidate for use as
> a base media type on which to build more complex capabilities. An additional media type is
> planned for the future which will add &lt;interaction metadata&gt; on top of HAL.

The HAL standard has not yet been enhanced with such interaction metadata, but, with the background knowledge of the
hypermedia cache pattern, one can very easily extrapolate to adding a similar _optional_ object in a representation
which is designed to contain such metadata.

This is what we have chosen to do in the API guidelines.

Specifically, we have added the reserved optional `_options` property to the HAL base, which is intended to
contain the "full response that would be obtained by sending an OPTIONS request to the `self` link of the
representation at the time the representation was generated"

With this "metadata cache pattern", before making an OPTIONS call on a resource for which the client already
has a representation, the client could check to see if the `_options` object exists in that representation.
If so, the client would use it; if not, the client can send an OPTIONS request to the `self` link.

When coupled with the `_embedded` property, it would become possible to provide the entire current state of a
resource network and the interaction metadata for each resource within that network, in one response.

When coupled with the Content-Location pattern, updating a resource will also return the new OPTIONS for that
resource too (and that of any embedded resources if the hypermedia cache pattern is also being exploited).

The result is big reduction in extraneous OPTIONS requests (possibly to zero), coupled with a (probably smaller)
reduction in GET requests.

#### Caveats
This pattern does not really suffer from the same problems as the hypermedia cache pattern because the
metadata details contained in the `_options` object do pertain only to this resource in its current state and can,
in fact, be viewed as a valid part of the state of the resource. Consequently it does not have issues interacting
with the standard HTTP caching and conditional request mechanisms.

The complexity of adding this data to the representation is, of course, implementation specific, but should
simply be a case of invoking the internal process for generating an OPTIONS response for the resource
whenever the resource representation is to be returned, thus, in principle, not very complex at all.

### Conclusion

These three patterns offer some powerful mechanisms for reducing the chattiness of API interactions. In terms
of implementation ease, the recommendation is to address them in the following order:

* Content-Location Pattern: As a standard feature of HTTP, this is non-controversial. What does surprise many developers is
the fact that in strict HTTP, the basic response to an update request is NOT the updated representation, but a status report
of the outcome of the request. Consequently many developers already return the updated representation, unaware that this
isn't strictly correct. The result, however, is that turning such default behaviour into a valid HTTP exchange involves little
more than just adding the `Content-Location` header to the existing response.
* Metadata Cache Pattern: If a resource engine is already responding to the OPTIONS method then simply embedding what
_would be_ the OPTIONS response into any generated resource representation is probably a "quick win" - simply invoke that
logic while preparing the representation and embed the response in the `_options` object. So this too should be fairly
straightforward to implement.
* Hypermedia Cache Pattern: The full use of this pattern can entail some fairly extensive modifications to the basic resource
management role of the back-end engine. However implementing the basic use cases described in the [When to embed] section
should be fairly straightforward and provide immediate gain benefits for clients the recognise the pattern.

An interesting aspect of these patterns is that it doesn't necessarily have to be the resource
engine (the SOR) itself that is modified to provide them. Instead some component in the integration path between an API Gateway
and the back-end resource engine can be used to enhance the basic response from that engine.

* For the Content-Location pattern, this component could perform the subsequent GET after a successful update request and return
that instead of the base response from the engine.
* For the Metadata Cache Pattern, that component could equally well perform a subsequent OPTIONS request when the engine has returned
a basic representation, and embed the response into that basic representation.
* For the Hypermedia Cache Pattern, the component can be coded to recognise certain patterns and perform GETs on interesting links in
a returned representation in order to exploit embedding effectively.
