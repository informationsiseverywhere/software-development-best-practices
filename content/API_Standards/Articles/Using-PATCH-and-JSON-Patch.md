## Understanding the PATCH verb

{{% notice note %}}
*Byline*: **PATCH is WAY more powerful than you think.**
{{% /notice %}}

Simply stated, `PATCH` is the HTTP method introduced by [RFC 5789](https://tools.ietf.org/html/rfc5789) that allows 
an API consumer to perform delta updates to a resource without having to overload `POST` or handle "complete" 
representations when updating through `PUT`. The concept is fairly intuitive.

* You have, say, a person resource who's full JSON representation is:

    ```
    {
        "name": "Fred Blogs",
        "occupation": "Professional Generic Identity Proider",
        "date_of_birth": "1 January 1970",
        "children": [
            "Bar Blogs",
            "Foo Blogs"
        ]
    }
    ```

* You note that there is a spelling mistake in the occupation property value, so you fire off a `PATCH` 
request with the following body:

    ```
    {
        "occupation": "Professional Generic Identity Provider"
    }    
    ```

The first thing to note is that the `PATCH` request body ***does not have to be in JSON*** just because the original
fetched representation was rendered in JSON. The server *may* support `PATCH` bodies with a `Content-Type` 
of `application/x-www-form-urlencoded` (i.e. HTML forms), in which case the `PATCH` request body could have been:

```
occupation=Professional%20Generic%20Identity%20Provider
```

or, if the server supports `application/xml`, in XML.

Such format options can be advertised in the `OPTIONS` response JSON Hyper-schema data (see later).

The general point is that, in this simple view of the use of `PATCH`, the client specifies a partial list of resource 
member properties and their new values.

However, that is not the whole story, and leaving things at this point risks missing some powerful features of `PATCH` that
can radically enhance the expressiveness and flexibility of an API that supports `PATCH`.

There is a bit of history behind `PATCH` that may escape the casual observer/reader, but will
be familiar to open source developers who submit "patches" to software projects. Linux Kernel patches, for example,
(before the advent of GitHub and pull requests) were submitted in the form of `diff` files, and applied using a `patch`
command. 

Even now the default view of a pull request on GitHub is that same `diff` format.

At its core the `diff` command creates a "script" that tells the `patch` tool how to update a file. And there's 
quite a bit of leeway that can be built into that script - it isn't just 

> at line 254 insert these lines

it's much more like 

> at around line 254 (give or take 5 or 6 lines) you'll find some lines like <this>: remove the last two and put <these> 
> in there instead. Then, once you've done that, a bit further down the file (about 20 lines further, give or take),
> there are a couple of lines that look like <this> - remove them. 
>
> If you can't do all of that don't do any of it.

This is the stable of thought from which the `PATCH` verb came, and careful reading of the RFC reveals this. In
particular the RFC does not say:

> PATCH applies a partial representation as a delta to the full resource

It says:

> The PATCH method requests that a set of changes described in the request entity be
> applied to the resource identified by the Request-URI. The set of changes is represented in
> a format called a "patch document" identified by a media type.

and, further on

> With PATCH ... the enclosed entity contains a set of instructions describing how a resource
> currently residing on the origin server should be modified to produce a new version. The
> PATCH method affects the resource identified by the Request-URI, and it also MAY have
> side effects on other resources; i.e., new resources may be created, or existing ones
> modified, by the application of a PATCH.

The simplistic approach above remains valid - it is, indeed, a simple example of a set of instructions to
modify the target resource - but that's not the end of it. While the `PATCH` RFC goes to some lengths to avoid
imposing a default patch document format, and mentions often that differing needs will certainly impose
different formats, there is another [RFC, 6902](https://tools.ietf.org/html/rfc6902), that really drives the point 
home. 

That RFC is titled: **JavaScript Object Notation (JSON) Patch**. In effect it describes the `diff` format 
expressed in JSON. 

Below are two alternatives of the previous `PATCH`, using JSON Patch instead of simple JSON:

* firstly one that demonstrates the power of the format, at the expense of simplicity:

    ```
    [
        { "op": "test", "path": "/occupation", "value": "Professional Generic Identity Proider", },
        { "op": "remove", "path": "/occupation" },
        { "op": "add", "path": "/occupation", "value": "Professional Generic Identity Provider" }
    ]
    ```

* secondly the more "sensible" approach

    ```
    [
        { "op": "replace", "path": "/occupation", "value": "Professional Generic Identity Provider" }
    ]
    ```

The JSON Patch RFC refers to the target resource being a JSON document but there really is no need for such
a distinction - as long as the origin server can sensibly interpret the semantics of the patch document, it really
doesn't matter what the underlying resource storage is, nor in what representation format the resource was
originally retrieved. What matters, as in ALL aspects of a REST API, is the clear and consistent conveyance
and understanding of client intent.

Other `PATCH` format RFCs exist. For example, [JSON Merge Patch](https://tools.ietf.org/html/rfc7386), which is much closer to the
simple approach above, and [XML Patch](https://tools.ietf.org/html/rfc7351) - which has a fairly obvious target.
However, the point should now be clear:

*the patch (request body) document can be far more sophisticated than one might initially appreciate.*

## What format to use

The short answer is "whatever makes sense".

There are bloggers who insist that JSON Patch is the ONLY choice and everything else is wrong, but both the `PATCH` and the 
JSON Patch RFCs militate against that. 

Real file resources (HTML files, for example, or program source files) would probably support the
exact format generated by the diff tool. 

One can even imagine the approach of supporting an entire programming language, such as Python or Groovy. However, 
the [Shell Shock exploit](https://en.wikipedia.org/wiki/Shellshock_%28software_bug%29) was based on the unfettered
use of a scripting language, so such an approach would be fraught with dangers and is forcibly not recommended.

However, while "flat" resources are well served by the simple approach, JSON Patch really comes into its own when
dealing with "structured" resources - i.e. resources that have nested data objects and lists. In the original example 
if the example person were to have another child, the simple `PATCH` approach would require the following patch document
to be sent:

```
{
    "children": [
        "Bar Blogs",
        "Foo Blogs",
        "Baz Blogs"
    ]
}
```

that is, the client must pass the full list with the new value added. This obviously becomes a burden when that list
is very large - and probably impossible if the list is subject to representation paging.

However, with JSON Patch the patch document can look like this:

```
[
    { "op": "add", "path": "/children/-", "value": "Baz Blogs" }
]
```

The difference becomes even clearer when all the client wishes to do is *modify* a value that is already in a list. Again
using the simple approach:

```
{
    "children": [
        "Bar Blogs",
        "Foo Doe",
        "Baz Blogs"
    ]
}
```

compared to using JSON Patch

```
[
    { "op": "replace", "path": "/children/1", "value": "Foo Doe" }
]
```

The recommendation, then would be to always support the simple approach, but to also introduce the use of JSON Patch into 
DXCs API implementations *where it makes sense*.

## Updating multi-valued relations

If we change the example resource here, another very interesting aspect to JSON Patch becomes evident:

```
{
    "_links" {
        "self": { "href": "/persons/fred-blogs-1" },
        "kid": [
            { "href": "/persons/bar-blogs-1" },
            { "href": "/persons/foo-blogs-1" }
        ]
    },
    "name": "Fred Blogs",
    "occupation": "Professional Generic Identity Provider",
    "date_of_birth": "1 January 1970"
}

```

To be clear, in this case we now have the more sophisticated model of the (literal) parent person resource
containing hypermedia links to the (literal) child person resources. The relation name `kid` is used here
to avoid confusion of the standard, resource modelling-related `child` relation name.

To add a new `kid` relationship to this resource, we can execute the following `PATCH`:

```
[
    { "op": "add", "path": "/_links/kid/-", "value": { "href": "/persons/baz-blogs-1" } }
]
```

This is a crystal clear expression of the client intent: 

> I wish to add Baz Blogs as a (literal) child of Fred Blogs 

The server will, of course, check that the passed href is valid, as well as the specified relation name `kid` before
applying the patch. 

**The key effect is that this avoids an artificial collection resource.** 

There is no need to traverse a single-valued `kids` relation that gives a Collection resource within which you find the 
persons who are the kids of Fred Blogs and to which you can perform some kind of specialised `PATCH` - or even execute a controller - 
to affect the list of children. All such actions can be executed where they most make sense, using semantics that make simple sense.

## How to advertise the availability of JSON Patch

The original `PATCH` RFC gives us one answer to this question. It introduces a new response header for `OPTIONS` responses: `Accept-Patch`, 
whose value should be a list of media types that the target resource of the `OPTIONS` request can accept in `PATCH` requests, e.g:

```
Accept-Patch: application/json, application/json-patch+json, application/x-www-form-urlencoded
```

This is a possibility - it is clear that `PATCH`es (in this case) can be made using simple JSON documents, JSON Patch documents, and HTML form documents.

However, no equivalent header is available for `PUT` and `POST` requests, which makes the approach a little arbitrary and inconsistent.

A more flexible and consistent approach, and the recommendation of DXC APIs, is simply to use *existing* capabilities of JSON Hyper-Schema 
interaction link descriptors which are already part of the `OPTIONS` response bodies. Using the `encType` property, the descriptor
can indicate the media type of the body for `PATCH` request (and also for `POST` and `PUT` requests). 

Extending an example from the [Guidelines document](../Guidelines.md#example-2---a-document-resource),
if the target document resource allows for both simple JSON patch documents and JSON Patch patch documents, then the following descriptor 
might be *added* to the `OPTIONS` response JSON Hyper-Schema data:

```
...

    {
        "rel": "update-diff",
        "title": "Modify the person",
        "mediaType": "application/json",
        "href": "http://example.api.com/persons/12345",
        "method": "PATCH",
        "encType" "application/json-patch+json"
        "schema": {
            ...
        }
    }
``` 

In this example, the `schema` is left deliberately vague. In effect, the schema for an `application/json-patch+json` document is "expected external
knowledge" - i.e. that specific media type has very specific requirements that, when the developer encounters it, they are expected to research (similarly
we do not "describe" the media type `application/json` - the developer is expected to know and/or research the JSON format for themselves).

However, this *may not* be adequate for conveying the specific needs for a particular resource (e.g. which properties actually can be updated in 
this manner) and so requires some more thought.

## Conclusion

* API implementers should understand and consider the full power of the semantics of the `PATCH` verb when designing their APIs, and not just assume the "simple delta" approach.
* For "flat" resources (or "top level" data in any resources), accept a simple "flat" expression of the `PATCH` "intent", such
as `application/json`, `application/xml`, or `application/x-www-form-urlencoded` patch documents.
* If the resource has embedded data structures, consider also implementing JSON Patch functionality
* Carefully review assumptions around the use of intermediate Collection resources. JSON Patch can make the use of such collections unnecessary. 
It is perfectly possible to directly manipulate multi-valued link relations with or without JSON Patch, but the use of JSON Patch makes the intent much clearer and 
provides far more flexibility than other approaches, making the need for intermediate collections in certain cases unnecessary (particularly when the Collection
is *not* a also a Factory resource.
* Use JSON Hyper-schema semantics to advertise the availability of JSON Patch semantics via the `encType` property.
* API consumer developers should become familiar with the JSON Patch format

Note that AWS's own APIs and command line interfaces make use of advanced `PATCH` semantics, as can be seen by the `--patch-operations` parameters to certain [AWS API Gateway update commands](https://docs.aws.amazon.com/cli/latest/reference/apigateway/index.html).



