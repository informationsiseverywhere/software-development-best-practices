## Epic Template

### Introduction
This is a modified table of the Scaled Agile Template which can be located at the following address. [LINK to Word Doc](http://v4.scaledagileframework.com/?wpdmact=process&did=MTA0LmhvdGxpbms=)


### Epic Business Case 

| Section | Content |
|---------|---------|
| Epic Name: | (Short name for the Epic) |
| Funnel Entry Date: | (Date the Epic entered the funnel) |
| Epic Owner: | (The name of the Epic Owner) |
| Epic Description: | (A description of the Epic; consider using the Epic Value Statement as a starting point.
See http://www.scaledagileframework.com/epics/) |
| Success Criteria: | (Describe how the success of the Epic will be measured, for example, 50% increase in shoppers under 25; Availability increases from 93% to 99.9%, etc.) |
| IN Scope: | (list of In scope areas) |
| Out of Scope: | (list of In scope areas) |
| NonFunctional Requirements: | List of Non Functional Requirements |
| Sponsors: | (List key business sponsors who will be supporting the initiative) |
| Users and Markets Affected: | (Describe the user community of the solution and any markets affected) |
| Products, Programs, Services Affected: | (Identify products, programs, services, teams, departments, etc. that will be impacted by this Epic) |
| Impact on Sales, Distribution, Deployment: | (Describe any impact on how the product is sold, distributed, or deployed) |
| Analysis Summary: | (Brief summary of the analysis that has been formed to create the business case.) |
| In-house or Outsourced Development: | (Provide recommendations for where the Epic should be developed) |
| Sequencing and Dependencies: | (Describe any constraints for sequencing the Epic and identify any potential dependencies with other Epics)|
| Milestones or Checkpoints: | (Identify potential milestones or checkpoints for reevaluation of the Epic) |
| Attachments / Links to : | (Other supporting documentation, links to other data, feasibility studies, models, market analysis, etc., that were used in the creation of the business case) |
| Other Notes and Comments: | (Any additional miscellaneous Information) |
