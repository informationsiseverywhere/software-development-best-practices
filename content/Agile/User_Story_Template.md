# User Story Template

### Characteristics of a User Story:

- A story should be complete and big enough to provide a user with some value. The user story should be user-centric, normally people write user story which is too much centric around component or system aspect, when writing a user story, we should focus on what the user is doing or getting out of the story.

- The goal is that when the user story is done, the user can do something of value to them.

- Group user stories which offer a feature in the same domain, or its good to group a certain feature or use case into a single Epic or even multiple Epics. Ideally you’ll break up your features in a way that you can launch into production parts of the feature independently from the whole, but it's not always possible.


### Acceptance Criteria

1. `If I do A.`
1. `B should happen.`


Also, here are a few points that need to be addressed:

1. `Constraint 1;`
1. `Constraint 2;`
1. `Constraint 3.`



### Resources:

* Mockups: `Here goes a URL to or the name of the mockup(s) in inVision etc;`
* Testing URL: `Here goes a URL to the testing branch or IP;`
* Staging URL: `Here goes a URL to the feature on staging;`


### Notes

`Some complementary notes if necessary`

* `Here goes a quote from an email`
* `Here goes whatever useful information can exist…`

