# iOS

## Development Environment
Xcode is the preferred IDE for developing iOS applications. It is freely available from the Mac App Store. An alternative IDE, AppCode from JetBrains exists, but it still does not support StoryBoards or XIBs, Xcode is still required to edit them. Xcode installation is an requirement to use AppCode, so at this time there is little advantage to using AppCode.

An Apple Developer account is no longer required to get started, but still necessary in order to publish apps either internally or from the App Store. See the [Store Submission page](./Store.md) for details on acquiring an account or publishing an app.

### Hardware
As with any development environment, more resources is better. Developers will be more productive with 16GB of memory and an SSD drive. A Mac Mini can serve as a starter environment for approximately $500 USD but full-time developers will outgrow this environment quickly.

An iOS developer should have at least two devices, an iPhone and an iPad in order to develop hardware-related features. See the [Emulator/Simulator section](./GeneralPrescription.md) for more detailed information regarding the iOS Simulator.

### Other Tools
Since your project should use a coding standard (see section below), [SwiftLint](https://github.com/realm/SwiftLint) is a good tool for managing coding standards. Swiftlint is easy for developers to use, and is scriptable as part of a continuous integration environment.

Since Xcode does not have a native package manager, [CocoaPods](https://cocoapods.org) is recommended to manage dependencies. CocoaPods has proven stable is in wide use in the iOS development community, and has an extensive set of available libraries.

## Language
iOS apps can be written in either Objective-C or Swift, but prefer Swift for new projects. Apple has chosen this language moving forward, and Swift has moved quickly into the mainstream. Swift has more potential for long term support with Apple, and Swift has fewer potential security issues than Objective-C. Apple is also fickle and quick to act, so there is the possibility that they may deprecate Objective-C at some point, without much warning.

## Coding Standards
To a certain extent, Xcode helps enforce some coding standards. A team needs to invest the effort to follow a style guide and be consistent, this will help future team members (or clients) to understand the code and be able to make changes easily. Don't re-invent the wheel. These are some good existing style guides:

- [Github Swift Style Guide](https://github.com/github/swift-style-guide) (Aligns well with SwiftLint)
- [Ray Wenderlich Swift Style Guide](https://github.com/raywenderlich/swift-style-guide)
- [Github Objective-C Style Guide](https://github.com/github/objective-c-style-guide)
- [Ray Wenderlich Objective-C Style Guide](https://github.com/raywenderlich/objective-c-style-guide)

Do not enforce your personal style on legacy code or code written in a different style. Follow the existing style, this will help the programmers that follow.

## Testing

### Unit Tests
Unit tests are required for all projects. In order for unit tests to be easy to write, most of the code should exist outside of the ViewController class. Despite iOS starting with a model-view-controller (MVC) architecture, the ViewController classes using this default architecture are coupled heavily to the view. While unit testing ViewControllers directly is possible, the process is complicated. An application architecture using Model-View-ViewModel (MVVM) and dependency injection provides a better framework for unit tests. The amount of code in the ViewController is minimized reducing the amount of code coupled to the view. More code lives in the ViewModel which is easier to unit test due to the lack of coupling to the ViewController.

![MVVM diagram](/static/images/mvvm-b27768df.png)
### UI
### Devices

iPhones and iPads cannot be flashed with older versions of the operating system. In order to maintain an adequate pool of devices for testing, a number of devices with varying screen sizes and operating system versions need to be available. Devices with the current OS and the previous version are necessary once a new version of iOS is ubiquitous. During the months directly after a release of a new  iOS, devices with the two previous versions are necessary.
### Performance
### Security

## Learning Resources
[Using CocoaPods](https://www.youtube.com/watch?v=iEAjvNRdZa0)
### MVVM
[MVVM for iOS](https://medium.com/@ramshandilya/lets-discuss-mvvm-for-ios-a7960c2f04c7)

[Introduction to MVVM](https://www.objc.io/issues/13-architecture/mvvm/)
