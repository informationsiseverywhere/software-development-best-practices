# Android
## Development Environment
Android Studio is the preferred development environment for Android. Eclipse is no longer viable as Eclipse support has been deprecated by Google.

A Java SDK is a requirement for installing Android Studio. Currently Java 8 is recommended.

Google does not require a developer account to develop Android applications, only for publishing apps. See the [Store Submission page](./Store.md) for details on publishing an app.

### Hardware
As with any development environment, more resources is better. Developers will be more productive with 16GB of memory and an SSD drive.

An Android developer should have at least two devices, a phone and a tablet in order to develop hardware-related features. Pixel and Nexus devices are useful because different versions of Android can be flashed to the same device. Unfortunately, few users have those devices so they are not as suitable for QA testing. See the [Emulator/Simulator section](./GeneralPrescription.md) for more detailed information regarding the Android Emulator.

## Language
Despite the announcement of Kotlin as a supported language for Android at Google I/O 2017, prefer Java at this time. There will be fewer resources and support for Kotlin in the short term. Clients and maintenance teams are unlikely to be familiar with Kotlin this early on in the language's maturity cycle with Android.

## Coding Standards
The general convention for Android code has been to follow the Android code base's style guide. A team needs to invest the effort to follow a style guide and be consistent, this will help future team members (or clients) to understand the code and be able to make changes easily. Don't re-invent the wheel. These are some good existing style guides:

- [Ray Wenderlich Java Style Guide](https://github.com/raywenderlich/java-style-guide)
- [Google's Android Contributor Style Guide](https://source.android.com/source/code-style)
- [Google's Java Style Guide](https://google.github.io/styleguide/javaguide.html)

Do not enforce your personal style on legacy code or code written in a different style. Follow the existing style, this will help the programmers that follow.

## Testing

### Unit Tests
Unit tests are required for all projects. In order for unit tests to be easy to write, most of the code should exist outside of the Activity or Fragment class. The default architectural classes for Android (Activities, Fragments) are coupled heavily to the user interface. While unit testing Activities/Fragments directly is possible, the process is complicated. An application architecture using  Model-View-Presenter (MVP) or Model-View-ViewModel (MVVM) along with dependency injection provides a better framework for creating unit testable code. The amount of code in the Activity/Fragment is minimized reducing the amount of code coupled to the view. More code lives in the Presenter or ViewModel which is easier to unit test due to the lack of coupling to the Activity/Fragment.

![MVP in Android](/static/images/MVP-in-android-1024x429.png)

### Hardware
Devices used for QA testing should come from more mainstream manufacturers like Samsung, LG, Sony or Motorola.
### Performance
### Security
### UI
## Learning Resources
