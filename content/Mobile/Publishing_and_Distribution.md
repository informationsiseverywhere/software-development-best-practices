# Publishing and Distributing Mobile Apps
Publishing to Apple and Google app stores takes a few simple steps:
-	access to their respective developer accounts
-	brand adherence and review 
-	a commitment to maintaining apps, ponce published

## Request Access
To get access, please contact [Faisal Siddiqi](mailto:fsiddiqi@dxc.com) and [Senthil Ponnuswamy](mailto:sponnuswamy@dxc.com) with:
-	a brief description of your app
-	accounts to which you’ll need access (iOS Developer, iOS Enterprise, Android).  Details below
-	names and DXC email addresses of developers that will need access
-	minimal permission level you’ll need 

## Brand Review
Early in the design process, please consult [DXC brand guidelines](https://my.dxc.com/brand-guide.html "Brand Guide") 
then contact [Alexandra Lucas](mailto:alucas22@dxc.com) for branding review – this makes publishing easier

# iOS
[iOS Permission Levels](https://developer.apple.com/support/roles/ "iOS")
![Which one do i need?](/static/images/MobileAppFlowchart.png)

# Android

[Android Permission Levels](https://support.google.com/googleplay/android-developer/answer/2528691?hl=en, "Android")
