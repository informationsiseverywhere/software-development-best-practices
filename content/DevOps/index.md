![AO Office Logo](/static/images/dxclogo.png)

# DevOps

As a technology provider, DevOps for DXC has meaning for development of our IP, how we deliver for our customers and also represents a capability that our delivery comes into contact with as we engage with our clients.

Consideration should be made for:
 - Build
 - Sell
 - Deliver

## What is DevOps

1: DevOps is “a cross-disciplinary community of practice dedicated to the study of building, evolving and operating rapidly-changing resilient systems at scale.” )(Jez Humble)

2: DevOps is the practice of operations and development engineers participating together in the entire service lifecycle, from design through the development process to production support. (The agile Admin)

![DevOps Image](/static/images/devops-process.png)

![DevOps Model](/static/images/devops-model.png)

## DXC Move to DevOps (Why, When, Who)

## DevOps in DXC's Build / Sell / Deliver

## DevSecOps (What is Sec)

`Sec` refers to Security, but within DevOps you can consider security in a few ways, driving to build Security as Code.

1. Integration of Security upfront within the software, as typically in times gone by this has always been an afterthought.

2. Security testing, code needs to be tested for security weaknesses, standard testing validates functionality, performance and integration with other systems. As part of the iterative cycle.

## What are Values / Principles / Methods / Practices and Tools

### DevOps Values –
I believe the fundamental DevOps values are effectively captured in the Agile Manifesto – with perhaps one slight emendation to focus on the overall service or software fully delivered to the customer instead of simply “working software.” Some previous definitions of DevOps, like Alex Honor’s “People over Process over Tools,” echo basic Agile Manifesto statements and urge dev+ops collaboration.

### DevOps Principles –
There is not a single agreed upon list, but there are several widely accepted attempts – here’s John Willis coining “CAMS” and here’s James Turnbull giving his own definition at this level. “Infrastructure as code” is a commonly cited DevOps principle. I’ve made a cut at “DevOps’ing” the existing Agile manifesto and principles here. I personally believe that DevOps at the conceptual level is mainly just the widening of Agile’s principles to include systems and operations instead of stopping its concerns at code checkin.

### DevOps Methods –
 Scrum with operations, Kanban with operations, etc. (although usually with more focus on integrating ops with dev, QA, and product in the product teams). There are some more distinct methods, like Visible Ops-style change control and using the Incident Command System for incident response. The set of these methodologies are growing; a more thoughtful approach to monitoring is an area where common methodologies haven’t been well defined, for example.

### DevOps Practices –
Specific techniques used as part of implementing the above concepts and processes. Continuous integration and continuous deployment, “Give your developers a pager and put them on call,” using configuration management, metrics and monitoring schemes, a toolchain approach to tooling… Even using virtualization and cloud computing is a common practice used to accelerate change in the modern infrastructure world.

### DevOps Tools –
Tools you’d use in the commission of these principles. In the DevOps world there’s been an explosion of tools in release (jenkins, travis, teamcity), configuration management (puppet, chef, ansible, cfengine), orchestration (zookeeper, noah, mesos), monitoring, virtualization and containerization (AWS, OpenStack, vagrant, docker) and many more. While, as with Agile, it’s incorrect to say a tool is “a DevOps tool” in the sense that it will magically bring you DevOps, there are certainly specific tools being developed with the express goal of facilitating the above principles, methods, and practices, and a holistic understanding of DevOps should incorporate this layer.

## Agile Manifesto

Read about the [Agile Manifesto](http://agilemanifesto.org/iso/en/manifesto.html).

The Agile Manifesto is made up of four values and twelve principles shown below.

### Four Values

- Individuals and interactions over processes and tools

- Working software over comprehensive documentation

- Customer collaboration over contract negotiation

- Responding to change over following a plan

### Twelve Principles behind the Agile Manifesto

We follow these principles:

- Our highest priority is to satisfy the customer through early and continuous delivery of valuable software.

- Welcome changing requirements, even late in development. Agile processes harness change for the customer's competitive advantage.

- Deliver working software frequently, from a couple of weeks to a couple of months, with a preference to the shorter timescale.

- Business people and developers must work together daily throughout the project.

- Build projects around motivated individuals. Give them the environment and support they need, and trust them to get the job done.

- The most efficient and effective method of conveying information to and within a development team is face-to-face conversation.

- Working software is the primary measure of progress.

- Agile processes promote sustainable development. The sponsors, developers, and users should be able to maintain a constant pace indefinitely.

- Continuous attention to technical excellence and good design enhances agility.

- Simplicity--the art of maximizing the amount of work not done--is essential.

- The best architectures, requirements, and designs emerge from self-organizing teams.

- At regular intervals, the team reflects on how to become more effective, then tunes and adjusts its behavior accordingly.


#### DXC Thought Leadership

[DXC Agile Manifesto](https://www.dxc.technology/innovation/insights/143810-creating_the_agile_organization)

## When do I know DevOps is Working

## Useful Links

[DevOps Framework Pack](https://dxcportal.sharepoint.com/sites/ArchitectureOffice/_layouts/15/WopiFrame.aspx?sourcedoc=%7BC7C4F361-0711-459D-8CD0-444A5220B2C3%7D&file=DXC%20Enterprise%20DevOps%20Framework%20-%20Process,%20People%20and%20Tool%20Chains,%20Best%20Practices%20and%20Impact_14sep2017.pptx&action=default)
