## High Performing Teams

#### Aligning to Design Principle 11. [LINK to Design Principles](../Design%20Principles/)

-<b>Small:</b> The best performing teams are small. They also get to choose their own size and staffing. Temporary members are welcome.--

-<b>Goal oriented:</b> The team knows what it needs to complete, in what order by when. The team has a clear view of the primary and secondary objectives of the tasks and which can be dropped if needed.

-<b>Responsible:</b> The team is and feels responsible for completing the goals.

-<b>Trusted:</b> The team feels trusted and supported and is therefore empowered to make its own tactical decisions.

-<b>Agile:</b> The teams are able to act quickly to take advantage of changes to the deal.

-<b>Variable geometry:</b> The teams change size and shape as the bid progresses. They do this naturally.

-<b>Flexible:</b> There is no dogma about who does what within the team to get to the goal. Roles are clear and understood but not fixed to a single person. One person can play multiple roles on a deal.

-<b>Organization is irrelevant:</b> The team is organization blind and treats members equally across organizations and locations.

-<b>Support:</b> Capabilities are available on demand to support the team as requested in the way requested.

-<b>Tools:</b> Tools are quick, flexible and open to being misused!

-<b>Teams have strong opinions, lightly held.</b>
