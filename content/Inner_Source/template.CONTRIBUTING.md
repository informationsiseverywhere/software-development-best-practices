# CONTRIBUTING.md template

Contributing guides are helpful documents that communicate how people can
contribute to your inner source project. See the list of [CONTRIBUTING.md
Examples](example.CONTRIBUTING.md) to help you get started in building your own.

See also [CONTRIBUTING.md](/CONTRIBUTING.md) for this repository.

## How to get started

To use this template, create a **CONTRIBUTING.md** file in the top level of your
project, copy the template, and fill it out with your information. When you’re
done, make sure people see your **CONTRIBUTING.md** guide. You can reference it
in your **README.md**. Also, it will be automatically linked whenever a **Pull
Request** is opened.
* Link back to your contributing guide in your Readme. Create a section called
  "Contributing" and link to **CONTRIBUTING.md**.
    * "Thanks for your interest in contributing! There are many ways to
      contribute to this project. Get started here (link)."
* If you have a website for your project, add a section called "Contributing"
  and link to your contributing guide. Or copy-paste your guide into the website
  and link to them in your **CONTRIBUTING.md** file.
* Include link to Code of Conduct

## Accepted Contributions
* Report issues
* Fix issues
* Improve the documentation

## Contributing Code
* The best way to collaborate with the project contributors is through GitHub: <link to Project>
* For bug fixes or adding new feature, please create pull request
* For raising an issue and defect, please open GitHub issue

## Review and Feedback Process
* List the review process and timeline.
* List the communication channels

## Pull Requests
We encourage contributions from the community. No fix is too small. We strive to
process all pull requests as soon as possible and with constructive feedback. If
your pull request is not accepted at first, please try again after addressing
the feedback you received.

To make a pull request you will need access to the DXC GitHub Instance at https://github.dxc.com.

When contributing new content or changing existing content, the content must be
written according to our content best practices, which can be found in the [Link
to Project Roadmap].

### How to create pull request:
* **Fork** the repository
* **Review** project roadmap [link to project roadmap]
* Make **changes** on your local environment
* **Test** your changes
* Submit **Pull Request** to the master repo and provide brief description. The **Pull Request** will detail the changes for the owner
* **@Mention** the team/team members in the comment section so the notification is sent. This alerts the team members to review the code
* Wait for the repo admins to review the changes and provide **feedback**

### Finally, a couple of tips to help you write:

* A friendly, welcoming voice will go a long way in making people feel excited
  to contribute to your project. Don’t forget to explain terms or concepts that
  might be confusing to a newcomer.
* Use this as an opportunity to lay the ground rules that will make you happy.
  The purpose of writing a guide is to make your life easier. It’s okay to say
  "XYZ pull requests are not accepted" or "I am busy, expect 1-2 weeks to review
  and merge your PR". Clarifying your needs up front will make it much faster to
  review future contributions.
* Be concise. Adding too much detail might actually make people less likely to
  read your guide, which will defeat the purpose.

  Content source: https://github.com/nayafia
