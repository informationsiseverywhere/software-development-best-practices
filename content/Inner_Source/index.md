![Architecture Office](/static/images/dxclogo.png)

# Inner Source

## What exactly is Inner Source?

Inner Source is a process of collaborative development using open source project
practices, but conducted within the confines of a private enterprise environment
to reduce silos, embrace collaboration from different teams and re-use code
across teams and organizations.

Reference: Inner Source on [Wikipedia](https://en.wikipedia.org/wiki/Inner_source).

In one picture:

![Inner Source - Cow-create](/static/images/inner.source.jpg)

Benefits of Inner Source at DXC:
- One single plane of discovery for intellectual property such as technical
  documentation, code, scripts, automated tests
- Maximize re-use across teams and accounts
- Collaborate beyond team boundaries with a gated, industry standard process
- Connect with other DXC teams, colleagues
- Make your [professional
  profile](https://help.github.com/enterprise/user/articles/about-your-profile/)
  visible thanks to your project contributions
- Lend a helping hand, resolve issues, answer questions
- Share and re-use code snippets, single files or quick tutorials with
  [GISTs](https://github.dxc.com/gist/discover)
- Be proud: showcase your work by creating your repositories as public,
  visible across DXC

# Best Practices and Guidelines for DXC teams

1. Open: Everyone shares their work to get different opinions and different
   ideas to democratizing access, creating a level playing field for the open
   sharing of work, ideas, and feedback, and ensuring cultural and strategic
   alignment. In GitHub, we do so using "public" repositories. Public means that
   the GitHub repository is visible to users authenticated with DXC Global Pass.
   They are not available on the public internet.
2. Repository maintainers: Have the responsibilities of directing, guiding, and
   supporting the project community. Writing and maintaining rules for the
   codebase, reviewing code, participating in discussion posts and mentoring new
   collaborators.
3. Modularity: Embracing a modular design allows developers to work on different
   parts of a project at the same time. It also allows for smaller contributions
   and eases the review process. 
4. Project roadmap: Involves defining priorities and developing a backlog which
   is a collaborative to-do list where individual projects are posted. Using a
   backlog offers an organization a place to discuss and close deliverables.
5. House Rules for Guests: There should be a clear guidelines in place for
   guests and in order to contribute to code/project they must agree to an
   agreement or read documentation regarding how this organization handles code.
   A contributing agreement is a device that specifies the house rules to let
   contributors know what is required in order for the trusted committer to
   accept a code contribution. See [CONTRIBUTING](template.CONTRIBUTING.md)

# Guides

## Organizing Guidelines

How to organize your GitHub repositories and team members? The following will
help you understand some of the organization concepts.

### Organizations

* A GitHub **Organization** is designed to group repositories within a specific
  organization and allows for broader permissions systems. A description from
  the vendor is also provided
  [here](https://help.github.com/enterprise/user/articles/about-organizations/).
* Create an organization for the top level org and set up teams for individual
  project repositories.
* Keep the list of owners limited since this is the highest level of permission
  that can impact the organization in a substantial and permanent way. [List of
  Owner permissions](https://help.github.com/enterprise/user/articles/permission-levels-for-an-organization/)
* Provide all members of your organization admin access to their specific repositories.
* [Help on creating Organization and Team](https://help.github.com/enterprise/user/categories/setting-up-and-managing-organizations-and-teams/)

### Members

* Ask that your organization's **members** personalize their GitHub profile
  by adding picture, ensure that they have their name populated, and make their
  membership in the organization public.
* Include an email and URL in the organization's profile to better connect with
  your organization's efforts.

### Teams

* Create **Teams** for individual project repositories.  This allows finer
  management for granting permissions. Use teams to give specific **read**,
  **write**, or **admin** access to repositories.
* Contributors can **@mention** teams in the comment section for each pull
  request or issue which will send out notification to the team members.
* This provides each team member a way to review code and provide comments.
* Contributors can only choose one Assignee per pull request at this time.
  Creating teams and mentioning them in pull request provides a way to review
  code as a team.
* Team members can collaborate and communicate using comment feature for a
  specific pull request.

## README.md File Guidelines

This is a **README** file template for DXC teams usage. We encourage you to help
provide feedback and propose changes by creating a **Pull Request** for this
repository.

We recommend you use the `.md` file extension on your README file (i.e.
**README.md**) so that GitHub will render the **Markdown** style formatting.
Otherwise, it will show up as plain text. This [Mastering
Markdown](https://guides.github.com/features/mastering-markdown/) guide can help
you become proficient in writing in **Markdown** to style your text to be clean
and attractive.

See [template.README.md](template.README.md) for a template.

## CONTRIBUTING.md File Guidelines

* Let others know how they can [contribute](#guidelines-for-contributing-to-the-project)

## Example CODE_OF_CONDUCT.md

### Contributor Code of Conduct
In the interest of fostering an open and welcoming environment, we as
contributors and maintainers pledge to make participation in our project and our
community a harassment-free experience for everyone, regardless of  level of
experience.

### Examples of behavior that contributes to creating a positive environment
- Using welcoming and inclusive language
- Being respectful of differing viewpoints and experiences
- Gracefully accepting constructive criticism
- Focusing on what is best for the community
- Showing empathy towards other community members

### Examples of unacceptable behavior by participants
- Trolling, insulting/derogatory comments, and personal or political attacks
- Public or private harassment
- Publishing others' private information, such as a physical or electronic address, without explicit permission
- Other conduct which could reasonably be considered inappropriate in a professional setting

## Maintain a Project Roadmap

This file lists the Current and future Roadmap of the project. It is critical to list the backlog and release schedule. 

### Current Roadmap

  * Enhancements
  * Features
  * Defect Fixes
  * Release Dates

### Future Roadmap
  * Enhancements
  * Features
  * Defect Fixes
  * Release Dates

### Links to Backlog and Release Schedule
  * Include a link to the GitHub Backlog file
  * Include a link to the Release schedule file

### Language Capabilities
  * Technology currently used to create the solution
  * Future Technologies to be included
  
## Guidelines for Contributing to the project

Please take a moment to review this document in order to make the contribution
process easy and effective for all the team members participating in the solution.

Following these guidelines helps to communicate that you respect the time of
the developers managing and developing this project. In return,
they should reciprocate that respect in addressing your issue or assessing
patches and features.


### Using the issue tracker

The issue tracker is the preferred channel for [bug reports](#bug-reports),
[features requests](#feature-requests) and [submitting pull
requests](#pull-requests), but please respect the following restrictions:

* Please **do not** use the issue tracker for personal support requests 

* Please **do not** derail or troll issues. Keep the discussion on topic and
  respect the opinions of others.


### Bug reports

A bug is a _demonstrable problem_ that is caused by the code in the repository.
Good bug reports are extremely helpful - thank you!

Guidelines for bug reports:

1. **Use the GitHub issue search** &mdash; check if the issue has already been
   reported.

2. **Check if the issue has been fixed** &mdash; try to reproduce it using the
   latest `master` or development branch in the repository.

3. **Isolate the problem** &mdash; attach screen prints.

Please try to be as detailed as possible in your report. What is
your environment? What steps will reproduce the issue? What browser(s) and OS
experience the problem? What would you expect to be the outcome? All these
details will help people to fix any potential bugs.

Example:

> Short and descriptive example bug report title
>
> A summary of the issue and the browser/OS environment in which it occurs. If
> suitable, include the steps required to reproduce the bug.
>
> 1. This is the first step
> 2. This is the second step
> 3. Further steps, etc.
>>
> Any other information you want to share that is relevant to the issue being
> reported. This might include the lines of code that you have identified as
> causing the bug, and potential solutions (and your opinions on their
> merits).


### Feature requests

Feature requests are welcome. But take a moment to find out whether your idea
fits with the scope and aims of the project. It's up to *you* to make a strong
case to convince the project's developers of the merits of this feature. Please
provide as much detail and context as possible.


### Pull requests

Good pull requests - patches, improvements, new features - are a fantastic
help. They should remain focused in scope and avoid containing unrelated
commits.

**Please ask first** before embarking on any significant pull request (e.g.
implementing features, refactoring code, porting to a different language),
otherwise you risk spending a lot of time working on something that the
project's developers might not want to merge into the project.

Please review [Project Roadmap](#Maintain-a-Project-Roadmap) prior to working on major enhancements. 

Follow this process if you'd like your work considered for inclusion in the
project:
How to create pull request:

1. [Fork](http://help.github.com/fork-a-repo/) the project, clone your fork,
   and configure the remotes:
2. Review project roadmap [link to project roadmap]
3. Make changes on your local environment and test changes
4. Submit Pull request to the master repo and provide brief description. The
   Pull Request will detail the changes for the owner @Mention the team/team
   members in the comment section so the notification is sent. This alerts the
   team members to review the code Wait for the repo admins to review the
   changes and provide feedback

**IMPORTANT**: By submitting a patch, you agree to allow the project owner to
license your work under the same license as that used by the project.

  
## Terms of Usage Guidelines

### Strategic/Legal Requirements

#### For DXC proprietary code
- Make sure inclusion in a repo does not undermine IP strategy or roadmap, and
  that no trademark or patent disclosure or other issues would be created if
  another team releases this code (or releases the code before you do).
- Track copyright notices, whether there are any patent license concerns (e.g.,
  if any technology in question is subject to a third party patent license that
  needs to be tracked for royalty concerns, such as codecs).
- If teams would have ability to take DXC code that has any of these concerns,
  make sure these are called out so the team taking the code is aware of these
  concerns.
- Confidential information such as a passwords, SSH keys and customer data
  should be limited to only those employees who have a "need to know". 

#### For third party proprietary code
- Refer to [DXC Open Source
  Sharepoint](https://dxcportal.sharepoint.com/sites/opensource/SitePages/Home.aspx)
  for guidance on how to use or contribute to Open Source projects.
- Verify with Legal whether code can be made available in repo.
- Can code be modified in source form?
- Are there any trademark or support considerations regarding modifications?
- Does the license from the supplier allow modifications?
- Do modifications have to be licensed back or IP rights assigned to supplier?
  If so, who will track this?  Are any additional approvals required to modify?
- Are there any fee or royalty considerations if another team uses this code?
  If so, how will these be tracked?  Who has responsibility for monitoring this?
- Provide notice to other users of any license restrictions, fee or royalty
  considerations, etc. and any contact information for users with questions
  about these.
- For commercial third party open source (e.g., RHEL), verify with Legal whether
  there are any issues with making code available.
- E.g., any fee or royalty obligations if another team uses this code, any
  trademark or support obligations which would affect ability to modify?  If
  there are any fee or royalty issues, who will track these?

#### If you leverage others’ code, respect product owners
- If you want to make contributions, give back to the teams that publish their
  code and also make your changes available.
- If you have a suggestion, engage the team in the conversation, even if you
  plan to offer the code directly in a pull request.
- Let product owners know if you leverage their code (a courtesy phone call or
  email should suffice).
- If you fork product owners’ code, offer improvements back to them with pull
  requests.
- If you incorporate product owners’ code in a product, engage them to ensure
  you will not be undermining an IP strategy or roadmap.  Also verify that no
  trademark or patent disclosure issues would be created by using/releasing
  another team’s code (e.g., if a team has patentable material in the repo but
  doesn’t plan to release until after applications are on file, does your team
  have a different release schedule that might compromise the other teams’
  patenting activity?).
- If you fork code and change it, the originating team should know and will not
  be obligated to support it unless you have a prior agreement in place with
  that team.
- Identify whether there are any fee/royalty considerations for third party
  proprietary code or patent licenses.
- Who on your team is responsible for tracking/reporting these?
- Identify whether there are any license restrictions, trademark or support
  considerations which would affect how the code may be used, modified, and/or
  distributed.
- Who on your team is responsible for tracking/reporting these?
- Take any documentation/license information corresponding to the code you take.
- Follow DXC processes re: open source approvals.




