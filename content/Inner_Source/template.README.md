# Template README.md File

## Project Name

Name of your project.

## Project Description

Add a brief description of your project. Describe the purpose of this project
and list the intended users. Also list the audience who would benefit from the
project's features and functionality. Indicate why a user may want to reuse your
code and list [Terms of usage](guideline.terms_of_usage.md). Encourage users to
fork/clone your repository for review or contribution purposes.

## Links

* Link to the site (if applicable)
* Link to the documentation
* Link to data files

## Bugs and Roadmap

* Link to the bugs list
    * GitHub **Issues** are a great way to keep track of bugs and to link to
      code changes
    * If you track bugs in another system (i.e. Jira, ServiceNow, etc) provide
      a way for users to submit bugs. GitHub **Issues** can serve this purpose
* Link to [Project Roadmap](guideline.roadmap.md)

## Documentation

* If you have any documentation that is easily displayable in this **README**,
  place it here
    * Documentation that is easily accessible or viewable makes contributing to
      your project more attractive
* Make best-practice available for the functional test

## Installation & Configuration

* Add installation and configuration instructions.
* An example of the output

## Contributing

Encourage contribution and add a link to the **CONTRIBUTING.md** file. We have
provided a [Contributing Template](template.CONTRIBUTING.md) to help get you
started.

## License

Include the license and copyright verbiage if applicable.
