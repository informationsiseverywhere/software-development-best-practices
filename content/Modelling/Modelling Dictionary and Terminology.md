![Architecture Office](/static/images/dxclogo.png)

# Dictionary and Terminology  

### Dictionary  

|Term|Meaning|
| --- | --- |
|Architecture||
|Architecture||
|Architecture Building Block (ABB)||
|Baseline||
|Building Block||
|Cardinality|defines the multiplicity of a source and target elements in relationship. |
|Classifier||
|Connector||
|Diagram|a visual representation of information|
|Element||
|Governance||
|Instance||
|Meta Model||
|Metadata||
|Modeling|see Modelling|
|Model|A grouping of diagrams|
|Modelling||
|Modelling Patter||
|Multiplicity||
|Object||
|Pattern| see Modelling Pattern|
|Relationship||
|Repository||
|Solution Architecture||
|Solution Building Block (SBB)||
|Stakeholder||
|Stereotype||
|View||
|Viewpoint|Contextual Position or Opinion|  



### Terminology  

|Term|Meaning|
| --- | --- |
|Can|Describes a possible feature or behavior available to the user.|
|Deprecated|Items identified as deprecated may be removed in the next version of this standard.|
|Implementation-defined|Describes a value or behavior that is not defined by this standard but is selected by an implementor of a software tool. The value or behavior may vary among implementations that conform to this standard. A user should not rely on the existence of the value or behavior. The implementor shall document such a value or behavior so that it can be used correctly by a user.|
|May|Describes a feature or behavior that is optional. To avoid ambiguity, the opposite of “may” is expressed as “need not”, instead of “may not”.|
|Obsolescent|Certain features are obsolescent, which means that they may be considered for withdrawal in future versions of this standard. They are retained because of their widespread use, but their use is discouraged.|
|Shall|Describes a feature or behavior that is a requirement. To avoid ambiguity, do not use “must” as an alternative to “shall”.|
|Shall not|Describes a feature or behavior that is an absolute prohibition.|
|Should|Describes a feature or behavior that is recommended but not required.|
|Will|Same meaning as “shall”; “shall” is the preferred term.|    

Source: [OpenGroup ArchiMate 3 Documentation](http://pubs.opengroup.org/architecture/archimate3-doc/chap01.html#_Toc451757913)


### Naming conventions  

See Taxonomy

