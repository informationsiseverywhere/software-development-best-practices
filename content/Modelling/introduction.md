![Architecture Office](/static/images/dxclogo.png)
# Architecture modelling standards - an introduction
**Contributors**
-	Maros Zvolensky (maros.zvolensky@hpe.com)
-	Sam Phillimore (sam.phillimore@hpe.com)
-	Eugen Molnar (eugen.molnar@hpe.com)


## Purpose
The purpose of this document is to initiate discussions for companywide Architecture Modelling Standards.

## Scope
The scope of this document is to provide an introduction to the topic of architecture modelling in DXC, including vision, mission and a high level break down of the modelling standards. This document is not binding to anyone in DXC.

## Assumptions
It is assumed that the reader is familiar with architecture modelling but it is not a prerequisite.

## Audience
Architects, engineers, business analysts, project managers and anyone within an enterprise   

# Why we are doing it
Clear, unambiguous architecture modelling standards which are accessible and understood by stakeholders of the architecture is paramount. Hundreds of models are being created on a daily basis but many of these models/diagrams never get used because they are hard to interpret and understand. Thus, without architecture modelling standards, architecture will become incomprehensible for most audiences and the return on investment in the models will be limited. With a common approach to creating architecture documentation the organization can realize significant value from the architecture, such as improved communication and increased organization efficiency.

# Mission
To define architecture modelling standards and patterns in an all-encompassing DXC Modelling Standards Reference Architecture (MSRA), which will govern the documentation of architecture in DXC.  

# Vision
**For:** DXC Architects, Engineers, Business Analysts, Project Managers and others  
**Who:** Required standards and governance for documenting designs, process and other artefacts  
**The:** DXC Modelling Standards Reference Architecture  
**Is a:** DXC Architecture Modelling approach  
**That:** Provides clear guidelines, standards and modelling patterns for the entire enterprise architecture, spanning: strategy, implementation, migration, business, application, technology and much more   
**Unlike:** Current ways of working  
**Our approach:** Brings rigor and standardization to documentation  
**Which supports:**	The company strategy  
# Requirements  
* **R001** - The DXC MSRA must be product independent. It shall be usable independently of the product used to create architecture models  
* **R002** - The DXC MSRA must enable the creation and use of architecture and solution building block. This is needed to enable reusability of components (blocks) and use of different physical implementations in different cases.  
* **R003** - The DXC MSRA must be robust enough to cover and support any part of DXC portfolio  
* **R004 ** - The DXC MSRA must be strongly governed  

# DXC Modelling Standards Reference Architecture (MSRA)  
The DXC MSRA shall provide policy and direction for supporting of solution architectures. It shall be equally applicable across all DXC capabilities. It needs to serve as a reference foundation of concepts, components and their relationships for solution architectures.  
![Architecture Office](/static/images/MSRA_applied.png)  
There are 7 main components of the DX MSRA  
1.	DXC Strategic Purpose  
2.	Design Principles  
3.	Industry Standards and Specifications  
4.	Modelling Meta Model   
5.	Patterns and Style Guides  
6.	Governance Model  
7.	Dictionary  

![Architecture Office](/static/images/MSRA.png)	  

**DXC Strategic Purpose**  
Identifies key stakeholder, architecture areas owners, implementations with the customer needs in mind. Explains why the MSRA exists, how and where it shall be used. Describes subject areas and addressed concerns of stakeholders. Provides explanations for the strategic goals and objectives set by DXC  
**Design Principles**  
Set of high level principles that must be applied while using the MSRA. Each principle must have unique identifier, name, statement, rationale and implications.   
**Industry Standards and Specifications**  
Lists and describes Industry standards applied on the whole MSRA or MSRA areas. These may introduce constrains which needs to be followed and applied. Compliance statements must be clearly derived.  
**Modelling Meta Model**  
Describes and prescribes stereotypes and connectors used for different parts of architecture models.  
**Patterns and Style Guides**  
Patters shall describe how the stereotypes will be applied and used to model specific, reusable areas. Those shall become standards to assure consistency in different areas. The Style Guides provide guidance on how line styles, colors, elements alignments etc. shall be applied.  
**Governance Model**  
Defined process which will ensure strong governance of the DXC MSRA. This process will state all the stakeholders, governing body which will own the DXC MSRA, processes, review boards the it’s schedules.   
**Dictionary**  
Dictionary will ensure that common language will be used and any ambiguity will be avoided.   

# Strategy
TBD
