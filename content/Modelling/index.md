# DXC Modelling Standards

[Modelling Standard](introduction.md)

### Architecture Modeling Tools

*	There is no centrally provided tool
*	DXC are OpenGroup Platinum members and working to define new standards around Architecture ( AAF based )
*	The nascent standards are TOGAF ( Framework ) and Archimate ( Notation description )
*	As long as DXC are OpenGroup members DXC can use both Archimate and Archi ( OpenSource )
*	DXC can use GitHub as a repository for Archi models but it isn’t a multi user solution ( manual processes needed to deal with parallel development )
*	If you work on a project and the project wants to fund tools separately AO have no problem with leading tools like Orbus, SPARXEA or Alfabet ( there is a groundswell towards SPARXEA due to its notation coverage and cost )
*	If you need to do BPMN or UML then Archi can’t help you and you have to look for other tools ( ms Visio / Modelio )
*	If using Archimate AO suggest you restrict meta model usage to a subset of the model – for clarity of the model ( take a minimalist approach )
*	DXC work on standards in OpenGroup may result in modifications or recommendations for Archimate usage but early days yet
*	Don’t forget there are other OpenGroup standards ( IT4IT, OBA, ……..  )

### Contribution Guidelines

Each Pattern will reside under the appropriate folder located in the `ArchitectureOffice/Standards` sub folders, Each Standard will be structured around a common template located Template.

Images are supported and encouraged as part of the Standard content, Images themselves should be local links to image files stored under `./_images` folder which is located in each standard sub folder.

For all additions or standard creation, this should be Forking off the main repository from the Architecture Office Organization into your GitHub account. Updates should be made here and the 'Pull Request' back into the main repository should be performed when you have completed updates. Moderators off the source will then approve the changes and commit / reject the changes.

We strongly value user feedback and appreciate your questions, bug reports and feature requests.
