# DXC Current to Future State

This document looks to outline the future state aspirations of DXC's technologist community as we look to transform our clients to Digital, we have to operate and behave differently to the past. A big piece of this will be the culture aspects of the DXC employees.

Supporting documents includes
- DXC Technologist Roles
- Align to the Right Process

### DXC Transforming the Legacy

We have to recognise that DXC's operating model, does not support the move to high-performing teams and providing the teams with the ability to operate autonomously and without fear of failure.

| Dimension | Current State | Transformed State | Actions / Direction |
|----------|---------------|-------------------|---------------------|
| What is my role? | Unclear, Confused | Roles defined | Align to roles defined for successful DevOps teaming structure  [LINK](./DXC_Technologist_Roles.md) |
| Working Structure | Hero's / Individual | Teams / Teaming | Execute around high performing teams |
| How do I develop myself? | No Career Path (don't understand my role) | Clear career path with branches to new roles | Supported by roles, creating development pathways for each role |
| How do I gauge my knowledge | A10, PM10 | ITX / Guru, Master, White Belt -> Black Belt | Supported by roles and development |
| How am I measured by Leadership? | Yearly appraisals / KPIs | Outcomes (DXC or Client) | Reset metrics of success for employees, create outcome based |
| Collaboration | Share Nothing | Highly Collaborative | Reward / Recognise high contributing employees |
| Tooling | Isolated / Desktop tool | Collaborative Centralised Tools | Develop in a collaborative environment, on tools that allow code control governance |


### Organisation culture

| Pathological | Bureaucratic (current) | Generative (future) |
|--------------|--------------|------------|
| Low Cooperation | Modest Cooperation | High Cooperation |
| Messenger Shot | Messengers neglected | Messengers trained |
| Responsibilities Shirked | Narrow Responsibilities | Risks are Shared |
| Bridging Discouraged | Bridging Tolerated | Bridging Encouraged |
| Failure leads to scapegoating | Failure leads to Justice | Failure leads to inquiry |
| Novelty Crushed | Novelty leads to Problems | Novelty Leads to Implemented |

It is the opinion of this document that DXC are emerging out from a Pathological Culture, and moving into a Bureaucratic culture. We have programs of work which are driving the Generative Culture (Platform DXC is an example of this)

### LEF - Renaissance of the IT Organisation

The leading edge forum has released a paper [LINK](https://leadingedgeforum.com/publication/renaissance-of-the-it-organization-report/)on the renaissance of the IT Orgnization, this paper outline the three main stages as shown below in the diagram.

![LEF Matrix](/static/images/lefmatrix1.png)

Currently DXC is very much in the <b>A (Enterprise IT)</b>, it is the position and view of this paper, that DXC cannot easily transition to <b>C (Matrix Org)</b>, therefore this paper sets out the role to support the <b> B (Cloud IT Era)</b>, with a desired future state of <b>C</b> - Please challenge!!!
