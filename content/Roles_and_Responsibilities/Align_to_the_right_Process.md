# Aligning to the Process

### Digital Life Cycle

It is important to understand the lifecycle and framework these people will be operating within, note that this is different to traditional models which followed a very linear path. The new model supports three main methodologies / frameworks which need to work together.

![Design Thinking / Lean / Agile ](/static/images/dtleanagile.png)

  - <b>Design Thinking</b> - Design Thinking is a methodology used by designers to solve complex problems, and find desirable solutions for clients. A design mindset is not problem-focused, it’s solution focused and action oriented towards creating a preferred future. Design Thinking draws upon logic, imagination, intuition, and systemic reasoning, to explore possibilities of what could be—and to create desired outcomes that benefit the end user (the customer)

  - <b>Lean</b> - Lean manufacturing or lean production, often simply "lean", is a systematic method for waste minimization ("Muda") within a manufacturing system without sacrificing productivity. Lean also takes into account waste created through overburden ("Muri") and waste created through unevenness in work loads ("Mura"). Working from the perspective of the client who consumes a product or service, "value" is any action or process that a customer would be willing to pay for.

  - <b>Agile</b> - Agile software development describes an approach to software development under which requirements and solutions evolve through the collaborative effort of self-organizing and cross-functional teams and their customer(s)/end user(s).[1] It advocates adaptive planning, evolutionary development, early delivery, and continual improvement, and it encourages rapid and flexible response to change.


### When to Engage based on DXC Current Operating Model

It would be easy to say just bring all these SMEs together for every engagement, but this would actually not be successful. You have to consider the Pioneers, Settlers and Town Planners models, and then bring forward the right roles to form a team that can operate in various stages.

For example it is pointless bring structure / standard oriented people into a '<b>Pioneering</b>' activity, it would inhibit progress. likewise putting a pioneer into into a '<b>Town Planners</b>' world would not be a successful relationship.

So we have to consider the type of work and ensure we bring the right people in, based on the DXC Model of Build, Sell and Deliver we can start to align the roles.[LINK](./DXC_Technologist_Roles.md)

| Roles / lifecycle | Build (Labs) | Build (Offering)| Build (Solution) | Sell|  Deliver |
|-------------------|--------------|-----------------|------------------|---|---------|
| Corp CTO         | :white_check_mark: | :white_check_mark: | :white_check_mark: | :white_check_mark: | :white_check_mark: |
| Regional CTO     | :recycle: | :recycle: | :white_check_mark: | :white_check_mark: | :warning: |
| Offering / Ind CTO | :recycle: | :white_check_mark: | :white_check_mark: | :white_check_mark: | :warning: |
| Delivery CTO       | :recycle:| :recycle: | :recycle: | :recycle: | :white_check_mark: |
| Designer*           | :white_check_mark: | :white_check_mark: | :white_check_mark: | Consultant | :x: |
| Architect*         | :x:      | :white_check_mark: | :white_check_mark: |Consultant| :white_check_mark: |
| Engineer*          | :x: | :white_check_mark: | :white_check_mark: | :recycle: | :white_check_mark: |
| Developer         | :white_check_mark: | :white_check_mark: | :white_check_mark: | :x: | :white_check_mark: |
| Product Owner     | :white_check_mark: | :white_check_mark: | :warning: | :warning: | :warning: |
| Project Mgr*       | :x: | :white_check_mark: | :white_check_mark: | :x: | :white_check_mark: |  

__Key__  

 * :white_check_mark: Fully Engaged (inc Feedback)
 * :recycle: Feedback loop
 * :warning: by exception  
 
NOTE: Person may change  


### Alignment to DXC and external Methods, Frameworks and Operating Model.

Is it also important that we consider the various Engagements and Stages of development DXC has to consider, this is typically where the complications arise, as we cannot apply the same operating model to each stage.

This document will base its direction around the following dimensions.

  - Pioneers, Settlers and Town planners
  - Agile / Scaled Agile Technics
  - DevOps Methodology
  - High Performing Team Structure
  - DXC Build, Sell, Deliver model
  - DXC Product Line Management Framework
  - ASD - Solution Development Framework

So firstly we can try to align all these principles and frameworks to <b>DXC Build Sell and Deliver</b> operating model, so lets do some alignment and try to simplify.

| Framework | Build (Labs) | Build (Offering)| Build (Solution) | Sell | Deliver (Transform / Deploy) | Deliver (Run and Maintain)|
|-----------|-----------|-----------|-----------|-----------|-----------|-----------|
| Pioneers| :white_check_mark: | :x: | :x: | :x: | :x: | :x: |
| Settlers| :x: | :white_check_mark: | :white_check_mark: | :x: | :x: | :x: |
| Town Planners| :x: | :x: | :x: | :x: | :white_check_mark: | :white_check_mark: |
| Agile| :white_check_mark: | :white_check_mark: | :white_check_mark: | :x: | :x: | :x: |
| DevOps| :x: | :white_check_mark: | :white_check_mark: | :x: | :white_check_mark: | :white_check_mark: |
| Scaled Agile| :x: | :white_check_mark: | :white_check_mark: | :x: | :x: | :x: |
| Lean| :x: | :x: | :x: | :x: | :white_check_mark: | :white_check_mark: |
| Lean SixSigma| :x: | :x: | :x: | :x: | :white_check_mark: | :white_check_mark: |
| PLM| :x: | :white_check_mark: | :white_check_mark: | :white_check_mark: | :white_check_mark: | :white_check_mark: |
| ASD| :x: | :x: | :white_check_mark: | :white_check_mark: | :white_check_mark: | :white_check_mark: |


## Alignment to External Methods / Frameworks.

DXC has in some cases adopted some of the industry frameworks like SAFe (e.g. Platform DXC, PLM), it is useful to consider how the roles defined align to this.

The <b>Design Thinking, Lean & Agile</b> reference is there to ensure we consider the broader end to end engagement, as we tend to focus on the Agile aspect, with out considering the upfront stages to a successful engagement.

### Scaled Agile (Example Used is SAFe 4.5)

As companies like DXC look to align to some common frameworks, Scaled Agile is one that keeps coming up. Now to start with there are several Scaled Agile frameworks, DXC for certain programs of work has chosen SAFe, but other are available and similar in nature. It can be seen from the scaled agile framework that roles are aligned to certain layers in the framework, but they all work together and pass information back a forward to support the overall program of work.

*Caution* We are using SAFe as the example, it is not the purpose of this document to define DXC standards on Frameworks or Processes.

![SAFe Technologist](/static/images/safetech.png)

Linking Design Thinking to SAFe.

![SAFe & Design Thinking](/static/images/dtsafe.png)
