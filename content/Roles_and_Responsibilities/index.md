# Roles and Responsibilities

This repository contains documents which outline how DXC needs to structure it's employees and develop a culture of collaboration.

There are 4 main documents in this repo

- [Problem Statement](./Problem_Statement.md)
This document outlines the high-level problem statement for DXC.

- [Current and Future State](./Current_Future_State.md)
This document looks to outline the current and future state of DXC in order to fix the problem statement.

- [DXC Technologist roles](./DXC_Technologist_Roles.md)
This document outlines the DXC Roles and Responsibilities for our technologist community, it is focused on Architects , Developers and Engineers, but will be expanded to cover other roles.

- [Align Roles to the right Process](./Align_to_the_right_Process.md)
This document looks at the various processes / org structures and Frameworks DXC operates within, this doc aims to help align the roles to them.

### Contribution

:heart: We love contribution, these documents are in a continuous state of improvement, the initial content and structure is to be validated and challenged, Contribution can be done by

  - Raising an Issue on the content, also consider the answer to the issue raised
  - Fork, Edit, Pull Request
    - Fix a typo / grammar
    - Add detail
    - Improve a section
