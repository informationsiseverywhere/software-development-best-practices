# Defining the Roles of Technologists in a DXC Agile World

DXC, like all large Enterprise Organizations, have many roles to support the business and our clients. This document aims to outline the key roles that DXC recognizes and the functions they perform. This document is intended to align to the future direction of IT to ensure that [High Performing Teams](https://github.dxc.com/pages/ArchitectureOffice/Standards/High_Performing_Teams/High_Performing_Teams/) consist of the right people, and to describe how they should interact.


## Getting the Right Team (Consider Scrum Team Definition)

In the new world order, there is a requirement to develop High-Performing Teams which creates a more highly productive model and benefits the employee and employer. Within every team, you still need to assign roles based on skills (i.e. you don't have eight quarterbacks on the field at the same-time).

As you look at the roles, it is very important to consider how they each contribute to a team structure. To understand the structure needed, we have to agree on the operating model. Below is a common industry flow that starts with Design Thinking (Designer + Architecture), leading into Lean (Architecture + Engineering + Developer), and then finally we enter the Iterative Development cycle for scale (Engineering / Developer).

Digital Lifecycle -  [LINK](Align_to_the_right_Process.md/#digital-life-cycle)


## Transformational Leadership

In order to have a high-performing team, the team leadership needs to exhibit certain characteristics. Taken from the Book "Accelerate" by Nicole Forsgen, Jez Humble and Gene Kim, the following image gives a high-level indication as to the traits of the modern team leader.  

![Transformational Leadership](/static/images/translead.png)

As we define the Teams and the Roles, let's ensure we identify the modern transformational leaders.


## High Performing Team

- Small: The best performing teams are small. They also get to choose their own size and staffing. Temporary members are welcome.

- Goal oriented: The team knows what it needs to complete, in what order by when. The team has a clear view of the primary and secondary objectives of the tasks and which can be dropped if needed.

- Responsible: The team is and feels responsible for completing the goals.

- Trusted: The team feels trusted and supported and is therefore empowered to make its own tactical decisions.

- Agile: The teams are able to act quickly to take advantage of changes to the deal.

- Variable geometry: The teams change size and shape as the bid progresses. They do this naturally.

- Flexible: There is no dogma about who does what within the team to get to the goal. Roles are clear and understood but not fixed to a single person. One person can play multiple roles on a deal.

- Organization is irrelevant: The team is organization blind and treats members equally across organizations and locations.

- Support: Capabilities are available on demand to support the team as requested in the way requested.

- Tools: Tools are quick, flexible and open to being misused!

- Teams have strong opinions, lightly held.


## Capability vs Role

Before we step into defining roles, it would be sensible to consider when you need a full-time role verse a set of capabilities. Once we start defining roles in an organisation boundaries, leadership structure etc etc. \[sic\]

An example of capability vs role is evident when we consider the journey a product takes from prototype to industrialised. Based on **DXC's Leading Edge Forum** research around [Pioneer, Settler and Town Planner](http://wardleypedia.org/mediawiki/index.php/Pioneers_settlers_town_planners) and the overlay of Simon Wardley's **Wardley Mapping**.

Let’s consider architecture. In the pioneering stages of developing a new service/solution/product, you would only have a very small team of resources maybe even just an individual. They would have multiple capabilities as they are only developing the prototype / simulation level. Therefore architecture would be a capability and not a defined role.

As you move the product/service through the development from Settler (MVP) to Town Planner (industrialised), the need to have more defined roles is required. People are still multi-skilled but they have a defined role and function which aligns the to how a high-performing team would be structured.

Note: DXC operates heavily in the Town Planner mode. We are establishing our Settler motion with DTCs, Luxoft who develop MVPs / Prototypes, etc, and we have smaller set of pioneering work e.g. DXC Labs.

![Capability vs Role](/static/images/capvsrole.png)

## DXC Roles

**\*\*\* Warning \*\*\*** We should recognise the role titles and descriptions, however DXC today confuses / misaligns these roles in lots of places. The purpose of this document is to clarify the roles moving forward.

We are initially focused on the Architect, Engineer and Developer. But other roles need to be clearly defined to support the end-to-end value chain; these include Product Owner, Scrum Master, and CTO for example.


| Primary Role | Description |
|-------|-------------|
| Designer | Design thinking gives us a deeper description of a problem, enabling us to prototype a range of potential solutions rapidly, and identify the right solution robustly. Design thinking will focus on five key disciplines areas Empathy, Integrative thinking, Optimism, Experimentation and Collaboration - Considered the Human dimension of the solution |  
| Architect | Architects work with business stakeholders and Solution and System Architects to implement technology initiatives across Value Streams. They rely on continuous feedback, foster adaptive design, and engineering practices, and drive programs and teams around to rally a common technical vision. - Considered the Logical / Mechanical Dimension of the design |
| Developer | Software developers are responsible for the entire development process. They are the ones who collaborate with the client to create a theoretical design. One or more Software developers create the code needed to run the software properly, test and fix problems. Software developers provide project leadership and technical guidance along every stage of the software development life cycle. |
| Engineer | Engineers carry out a variety of tasks. Their jobs require a combination of abilities, including system design and analysis as well as communication, mathematical and business skills. These professionals work with data and project managers to understand systems and consult with customers to determine their needs. They implement new systems, correct software errors in existing systems and improve performance through hardware/software upgrades.|
| Product Manager / Owner| Product Management has content authority for the Program Backlog / Roadmap. They are responsible for identifying P&L of the product, prioritising Features, guiding the work through the Program Kanban and developing the program Vision and Roadmap. Product Owners will be responsible for the Build, Sell and Delivery success of the products |


### How These Roles Interact

It is not expected that these roles operate in isolation to the others. The team structure is pivotal to success. The purpose of table is to ensure that each role understands how it helps and supports the others without impacting their ability to operate in an agile / continuous fashion.


| Designer | Architect | Engineer | Developer |
| ---------|---------------|-----------|----------|
| Design based on human need | Translate Business Need into logical / mechanical blueprints | Translate Blueprints into Releases / Sprints| Develop Release |
| Prototype and ideate | Develop Epics / User Stories | Develop Solution Designs / Activities to execute User Stories | Develop iterative elements of the Users stories based on defined sprints |
| Understand users context | Design to Standards / Business requirement | Define Technology / Tools to Support the Standards and Business requirements | Develop within any defined guardrails or principles applied by the engineers and architects|
| Pilot and test Human interactions | Outline Design Test Criteria | Create Test Plans / QA | Execute Tests during Development process |
| Design for user experience and Interface | Design for Integration to Other systems and Services | Define Integration methods and services | Develop to Integration needs as defined |
| Define the non-logical dimensions (human interaction) | Outline Business Domains and boundaries for Services based on best practices |  Define the criteria for Developers to meet the domains | Develop the services |
| Design for Human experience | Design to Compliance for client Business / regulatory | Translate compliance to test criteria for Developers | Develop to Test Criteria |
| Research and Empathise / Innovate | Have awareness of Tools and Technologies / make recommendations | Provide clarity on options based on any client criteria / operational alignment | Freedom to work within the engineering alignment |
| Design responds to prototyping and architecture response | Process Feedback into Architecture | Process Feedback into Program and DevTeam Instructions , and provide feedback into to design / portfolio | Provide Feedback into Design/ portfolio and engineering |

### Description of Roles Encompassed by Architect, Engineer and Developer

#### Architect Roles and Descriptions

| Architecting Sub Roles | Description | Other DXC Titles |
|-------|-------------|-----|
|Enterprise Architect | An enterprise architect is an enterprise architecture specialist that works closely with stakeholders, including management and subject matter experts (SME), to develop a view of an organization's strategy, information, processes and IT assets. An enterprise architect is responsible for using this knowledge to ensure IT and business alignment. An enterprise architect connects an organization’s business mission, methodology and processes to its IT strategy and established in-depth documentation with the help of an array of architectural models, or views, which provide a picture of how an organization's existing and future requirements may be accomplished in an effective, agile, sustainable and flexible manner.| Solution Consultant, Digital Solution Designer, Account Architect, Industry Architect, Digital Solution Designer |
| Solution Architect | Architect/Engineering support solution development by providing, communicating and evolving the broader technology and architectural view of the solution. Architect/Engineering teams occur at both the Program and Large Solution Levels. <b>System Architect/Engineering</b> operates mainly in the context of the release train, where they work with Agile Teams and provide technical enablement concerning subsystems and capability areas for an release train. <b>Solution Architect/Engineering</b> teams provide technical leadership for evolving architectural capabilities of the entire solution. Both involve close collaboration with business stakeholders, teams, customers, suppliers, and third-party stakeholders in defining the technology infrastructure, decomposition into components and subsystems, and definition of interfaces between subsystems and between the solution and solution context. While providing a general view of solution architecture, Architect/Engineering enables those who implement value by empowering them to make local decisions, allowing a faster flow of work and better economics.| |
| Account Architect | Aligns to  Enterprise  / Solution Arch Profile, these people are  assigned to support a specific DXC Client. | |
| Industry / Business Architect | Aligns to  Enterprise  / Solution Arch Profile + Vertical or Business domain specific knowledge| |
| Domain Architect | * See Domain Architect Table |

**\*\*\* Observation \*\*\*** It is clear from the descriptions that a Systems Engineer and Solution Architect could be interchangeable roles. As the functions are very similar, it will most likely be a preference of choice for the individual / business to decide how they want to classify themselves.

#### Domain Architects Table

| Domain | Description |
|--------|-------------|
|Information / Data | A information technology, data architect will compose of models, policies, rules or standards that govern which data is collected, and how it is stored, arranged, integrated, and put to use in data systems and in organizations.|
|Security| Security architects combine broad technical and security skills with strong business analysis, consultancy and communication skills. Using their broad experience and knowledge they influence the design of systems and services to mitigate architectural weaknesses and security risks, whilst always being mindful of the overarching purpose of the system.|
|Infrastructure / Cloud | Infrastructure / Cloud Architects design and implement information systems to support the enterprise infrastructure of an organization. They ensure that all systems are working at optimal levels and support the development of new technologies and system requirements.|
|Network| A computer network architect is someone who designs and builds data communication networks. These can include local area networks, wide area networks, and intranets, which can range from a small connection to a multinational series of globally distributed communications systems.|
|Process| A business architect is a practitioner of business architecture, a discipline concerned with developing and maintaining business capabilities of the enterprise in line with the corporate strategy as well as contributing to the business strategy and plans.|

#### Developer Roles & Descriptions

| Developer Sub Roles | Description |
|-------|-------------|
| Front-end Developer (Client-Side Developer)| This is a developer who specializes in the programming of visual user interfaces, including its aesthetics and layouts. A front-end developer code runs on a web browser, on the computer of the user of the site. It is very high-level work, normally far removed from the hardware. It requires an understanding of human-machine interaction and design principles more than computer science theory. Much of a front-end developer’s life is spent dealing with cross-browser compatibility issues and tweaking details of the visual presentation of a UI. Front-end development skills include the design of user interface (UI) and user experience (UX), CSS, JavaScript, HTML, and a growing collection of UI frameworks.|
| Backend Developer (Server-Side Developer)| This is a developer who specializes in the design, implementation, functional core logic, performance and scalability of a piece of software or system running on machines that are remote from the end-user. Back-end systems can grow to be very complex, but their complexity is often not visible to the users. For example, consider Google search engine. The front-end part is a very simple UI with a title, a text box, and two or three buttons. The backend is an enormously complex system, able to crawl the web, index it, and find what you are looking for with a growing array of sophisticated mechanisms. A back-end developer works with programming languages such as Java, C, C++, Ruby, Perl, Python, Scala, Go, etc. Back-end developers often need to integrate with a vast array of services such as databases, data storage systems, caching systems, logging systems, email systems, etc.|
| Full-stack Developer| This is a developer that does both front-end and back-end work. He or she has the skills required to create a fully functional web application.|
|Middle-Tier Developer| This is a developer who writes non-UI code that runs in a browser and often talking to non-core code running on a server. In general, middle tier is the “plumbing” of a system. The term middle-tier developer is used to describe someone who is not specialized in the front-end or the back-end but can do a bit of both, without being a full stack developer. Only rarely engineers have this as a title, as it is more of a description of a skill set than a career path.|
|Web Developer| Web developers are software engineers who specialize in creating websites. They are either front-end developers, back-end developers, middle-tier developers or full-stack developers. It has a low entry-point, requiring as little as basic HTML and CSS knowledge. With only a few months of experience, an entry-level web developer can start producing code that ships to production systems. It is a particularly attractive option for people who have no CS fundamentals and want to join the programming world.
|Desktop Developer| This is a developer who works on software applications that run natively on desktop operating systems (such as Mac OS, Windows, and Linux). Desktop developers often use GUI Toolkits such as Cocoa, XAML, WinForms, Gtk, etc.|
|Mobile Developer| This is a developer who writes code for applications that run natively on consumer mobile devices such as smartphones and tablets. Mobile development was almost unheard of before the early 2000s and the explosion of the smartphone market. Before then mobile development was considered a subset of embedded development. A mobile developer understands the intricacies of mobile operating systems such as iOS and Android, and the development environment and frameworks used to write software on those operating systems. That includes Java, Swift, and Objective-C.|
| Graphics Developer | This is a type of developer specialized in writing software for rendering, lighting, shadowing, shading, culling, and management of scenes. These developers are often responsible for integrating technologies in the gaming and video production industry. Graphic development used to be a form of low-level development, requiring advanced math and computer science training. It is becoming more accessible with the introduction of commercial and open source frameworks and systems. For example, very few people today need to be able to write a shader from scratch. Frameworks include DirectX, OpenGL, Unity 3D, WebGL. For more advanced graphic developers, low-level development requires C, C++, and Assembly.
|Data Scientist| This type of developer writes software programs to analyze data sets. They are often in charge of statistical analysis, machine learning, data visualization, and predictive modelling. Languages used by data scientists often include SQL, R, and Python.|
|Big Data Developer| This type of developer writes software programs to store and retrieve vast amounts of data in systems such as data warehouses, ETL (Extract Transform Load) systems, relational databases, data lakes management systems, etc. A big data developer is often familiar with frameworks and systems for distributed storage and processing of vast amounts of data such as MapReduce, Hadoop, and Spark. Languages used by Big Data Developers include SQL, Java, Python, and R.|
|DevOps Developer | This is a type of developer familiar with technologies required for the development of systems to build, deploy, integrate and administer back-end software and distributed systems. Technologies used by DevOps Engineers include Kubernetes, Docker, Apache Mesos, the HashiCorp stack (Terraform, Vagrant, Packer, Vault, Consul, Nomad), Jenkins, etc.|
| CRM Developer |This type of developer specializes in the field of systems that collect user and consumer data. These developers are tasked with improving customer satisfaction and sales by improving the tooling used by customer support representatives, account managers, and sale representatives. Technologies used by these developers include SAP, Salesforce, Sharepoint, and ERP (Enterprise Resource Planning).|
|Software Development Engineer in Test (SDET)| This type of developer is responsible for writing software to validate the quality of software systems. They create automated tests, tools and systems to make sure that products and processes run as expected. Technologies used by SDETs include Python, Ruby, and Selenium.|
| Embedded Developer |These developers work with hardware that isn’t commonly classified as computers. For example, microcontrollers, real-time systems, electronic interfaces, set-top boxes, consumer devices, iOT devices, hardware drivers, and serial data transmission fall into this category. Embedded developers often work with languages such as C, C++, Assembly, Java or proprietary technologies, frameworks, and toolkits.|
|High-Level Developer| This is a general term for a developer who writes code that is very far from the hardware, in high-level scripting languages such as PHP, Perl, Python, and Ruby. Web developers are often high-level developers, but not always.|
| Low-Level Developer |This is a general term for a developer who writes code that is very close to the hardware, in low-level languages such as assembly and C. Embedded developers are often low-level developers, but not always.|
|Security Developer|This type of developer specializes in creating systems, methods, and procedures to test the security of a software system and exploit and fix security flaws. This type of developer often work as “white-hat” ethical hacker and attempts to penetrate systems to discover vulnerabilities. Security developers most often write tools in scripting languages such as Python and Ruby and understand in details the many patterns used to attack software systems. More advanced security developers need to read and understand operating systems source code written in C and C++. They might also reverse engineer libraries and commercial software systems to find and exploit vulnerabilities.|

#### Engineering Roles and Descriptions

| Engineering Sub Roles | Description |
|-------|-------------|
| SRE Site Reliability Engineer | Site reliability engineers create a bridge between development and operations by applying a software engineering mindset to system administration topics. They split their time between operations/on-call duties and developing systems and software that help increase site reliability and performance. Google puts a lot of emphasis on SREs not spending more than 50% of their time on operations and considers any violation of this rule a sign of system ill-health.|
| System Engineer | System engineers carry out a variety of tasks. Their jobs require a combination of abilities, including system design and analysis as well as communication, mathematical and business skills. These professionals work with data and project managers to understand systems and consult with customers to determine their needs. They implement new systems, correct software errors in existing systems and improve performance through hardware/software upgrades.|
| Software Engineer | Software Engineer are in charge of the entire development process for a software program. They may begin by asking how the customer plans to use the software. They must identify the core functionality that users need from software programs. Software developers must also determine user requirements that are unrelated to the functions of the software, such as the level of security and performance need. Software engineering refers to the application of engineering principles to create software. Software engineers participate in the software development life cycle through connecting the client’s needs with applicable technology solutions. Thus, they systematically develop processes to provide specific functions. In the end, software engineering means using engineering concepts to develop software.|
| Data Engineer | Data engineers may work closely with data architects (to determine what data management systems are appropriate) and data scientists (to determine which data are needed for analysis). They often wrestle with problems associated with database integration and messy, unstructured data sets. Their ultimate aim is to provide clean, usable data to whomever may require it.|
| Release Engineer | Responsible for software builds and releases. Responsible for the design and development of builds, scripts, installation procedures, and systems including source code control and issue tracking. Works closely with a quality assurance team to ensure final version is up to organisational standards. Requires a bachelor's degree in a related area. Typically reports to a supervisor/manager. Typically requires 2 to 4 years of related experience. Gaining exposure to some of the complex tasks within the job function. Occasionally directed in several aspects of the work.|
| Test Engineer | QA engineer responsibilities include designing and implementing tests, debugging and defining corrective actions. You will also review system requirements and track quality assurance metrics (e.g. defect densities and open defect counts.) The QA Engineer role plays an important part in the product development process. Our ideal candidate will be responsible for conducting tests before product launches to ensure software runs smoothly and meets client needs, while being cost-effective.|

**\*\*\* Observation \*\*\*** It is clear from the descriptions a Systems Engineer and Solution Architect could be interchangeable roles. As the functions are very similar, it will most likely a preference of choice for the individual how they want to classify themselves.


## Other Critical Roles in an Agile Operation

| Other Roles | Description | Other / Sub Titles |
|-------|-------------|-----------|
| Scrum Master | Scrum Masters are servant leaders and coaches for an Agile Team. They help educate the team in Scrum, Extreme Programming (XP), Kanban, and SAFe, ensuring that the agreed Agile process is being followed. They also help remove impediments and foster an environment for high-performing team dynamics, continuous flow, and relentless improvement.| Project Manager |
| CTO/CT | Primarily is a Customer-facing CTO, who spends much of his or her time talking with end customers. The CTO role needs to focus on "executive-level conversations about 'Are we really building the right things?'" with a feedback look into the offering build organization. This role needs a CTO who can dive technically deep on customer needs, and also have the influence to "change your ways." In addition to the Customer Facing work, the CTO will help the DXC Executive and Technologist community to focus on the right technology areas to help grow the business, this will include development of People, Processes and New Technology.|

### DXC Digital Transformation Centre's Structure for Career Pathing

![DTC Career Path](/static/images/dtccareerpath.png)


### The Scrum Team

The Scrum Team consists of a Product Owner, the Development Team, and a Scrum Master. Scrum Teams are self-organizing and cross-functional. Self-organizing teams choose how best to accomplish their work, rather than being directed by others outside the team. Cross-functional teams have all competencies needed to accomplish the work without depending on others not part of the team. The team model in Scrum is designed to optimize flexibility, creativity, and productivity. The Scrum Team has proven itself to be increasingly effective for all the earlier stated uses, and any complex work.

Scrum Teams deliver products iteratively and incrementally, maximizing opportunities for feedback. Incremental deliveries of "Done" product ensure a potentially useful version of working product is always available.


### The Product Owner

The Product Owner is responsible for maximizing the value of the product resulting from work of the Development Team. How this is done may vary widely across organizations, Scrum Teams, and individuals.

The Product Owner is the sole person responsible for managing the Product Backlog. Product Backlog management includes:

- Clearly expressing Product Backlog items;
- Ordering the items in the Product Backlog to best achieve goals and missions;
- Optimizing the value of the work the Development Team performs;
- Ensuring that the Product Backlog is visible, transparent, and clear to all, and shows what the Scrum Team will work on next; and,
- Ensuring the Development Team understands items in the Product Backlog to the level needed.
- The Product Owner may do the above work, or have the Development Team do it. However, the Product Owner remains accountable.

The Product Owner is one person, not a committee. The Product Owner may represent the desires of a committee in the Product Backlog, but those wanting to change a Product Backlog item’s priority must address the Product Owner.

For the Product Owner to succeed, the entire organization must respect his or her decisions. The Product Owner’s decisions are visible in the content and ordering of the Product Backlog. No one can force the Development Team to work from a different set of requirements.

#### Product Owner Skills

- Business Acumen
- Technical Domain Knowledge
- Market Awareness
- Negotiation
- Persuasion
- Evangelism
- Communication: Written, Verbal, and Nonverbal
- Commercial
- Legal Issues and Contracts.
- Selling


### The Development Team

The Development Team consists of professionals who do the work of delivering a potentially releasable Increment of "Done" product at the end of each Sprint. A "Done" increment is required at the Sprint Review. Only members of the Development Team create the Increment.

Development Teams are structured and empowered by the organization to organize and manage their own work. The resulting synergy optimizes the Development Team’s overall efficiency and effectiveness.

Development Teams have the following characteristics:

- They are self-organizing. No one (not even the Scrum Master) tells the Development Team how to turn Product Backlog into Increments of potentially releasable functionality;
- Development Teams are cross-functional, with all the skills as a team necessary to create a product Increment;
- Scrum recognizes no titles for Development Team members, regardless of the work being performed by the person;
- Scrum recognizes no sub-teams in the Development Team, regardless of domains that need to be addressed like testing, architecture, operations, or business analysis; and,
- Individual Development Team members may have specialized skills and areas of focus, but accountability belongs to the Development Team as a whole.

#### Development Team Size

Optimal Development Team size is small enough to remain nimble and large enough to complete significant work within a Sprint. Fewer than three Development Team members decrease interaction and results in smaller productivity gains. Smaller Development Teams may encounter skill constraints during the Sprint, causing the Development Team to be unable to deliver a potentially releasable Increment. Having more than nine members requires too much coordination. Large Development Teams generate too much complexity for an empirical process to be useful. The Product Owner and Scrum Master roles are not included in this count unless they are also executing the work of the Sprint Backlog.


### The Scrum Master

The Scrum Master is responsible for promoting and supporting Scrum as defined in the Scrum Guide. Scrum Masters do this by helping everyone understand Scrum theory, practices, rules, and values.

The Scrum Master is a servant-leader for the Scrum Team. The Scrum Master helps those outside the Scrum Team understand which of their interactions with the Scrum Team are helpful and which aren’t. The Scrum Master helps everyone change these interactions to maximize the value created by the Scrum Team.

#### Scrum Master Service to the Product Owner

The Scrum Master serves the Product Owner in several ways, including:

- Ensuring that goals, scope, and product domain are understood by everyone on the Scrum Team as well as possible;
- Finding techniques for effective Product Backlog management;
- Helping the Scrum Team understand the need for clear and concise Product Backlog items;
- Understanding product planning in an empirical environment;
- Ensuring the Product Owner knows how to arrange the Product Backlog to maximize value;
- Understanding and practicing agility; and,
- Facilitating Scrum events as requested or needed.

#### Scrum Master Service to the Development Team

The Scrum Master serves the Development Team in several ways, including:

- Coaching the Development Team in self-organization and cross-functionality;
- Helping the Development Team to create high-value products;
- Removing impediments to the Development Team’s progress;
- Facilitating Scrum events as requested or needed; and,
- Coaching the Development Team in organizational environments in which Scrum is not yet fully adopted and understood.

#### Scrum Master Service to the Organization

The Scrum Master serves the organization in several ways, including:

- Leading and coaching the organization in its Scrum adoption;
- Planning Scrum implementations within the organization;
- Helping employees and stakeholders understand and enact Scrum and empirical product development;
- Causing change that increases the productivity of the Scrum Team; and,
- Working with other Scrum Masters to increase the effectiveness of the application of Scrum in the organization.

---

### Questions to ask:

- Do we still need Enterprise Architects, or just have EA Skills in certain Roles
 -- [An EA in the traditional 'TOGAF' sense of the role, probably not but in an increasingly autonomous, squad/pod led, value delivery focused swarm model for executing change you still need someone to hold the map and read the compass. Small federated teams of highly skilled and knowledgeable change agents delivering the right business feature in small incremental releases are a fantastic experience for a client that has only existed in 'Enterprise IT' but there is a necessary transition phase to be managed, and those small nimble teams aren't incented to care about the higher order strategic goals of a Global Business Unit. So in answer an EA might be necessary to evolve an account/sector from level 1 to 2/3 on a CMMI for example, then consider a segue to a senior technology leader that has EA skills and experience to own a BVF or re-orient an account/sector towards those macro SBO's and run overwatch]
- Do we remove Application Architect and replace with Software Engineer
- Do we accept the alignment of the model to a Scaled Agile framework
