### Team Skills

| Primary Role | Hard Skills | Soft Skills |
|-------|-------------|-----------|
|Business Analysts| Agile: Business Analysis: Business Improvement Process: Digital Perspective: Innovation: Requirements Definition Management: Stakeholder Mgmt: User Focus: Business Modelling: Business Process Testing: Ent / Biz Architecture: Methods and Tools: Testing |
| Designer | Curiosity > Observation > Empathy > Research > Sketching > Prototyping > Testing > Repeat |  |
| Architect | Data modelling: Understanding of frameworks: Knowledge of UML: Requirements analysis and management:  The ability to visualise complex issues: The ability to communicate across communities: Creative problem solving:  | |
| Developer | xx | Empathy: Communication: Team Work: Approachability & Helpfulness: Patience: Problem Solving: Accountability: Creativity: Time Management|
| Engineer | xxx| Empathy: Communication: Team Work: Approachability & Helpfulness: Patience: Problem Solving: Accountability: Creativity: Time Management |
| Product Manager / Owner / Lead (Squad)| Business Acumen: Technical Domain Knowledge: Market Awareness: Negotiation: Persuasion: Evangelist: Communication: Written, Verbal, and Nonverbal: Commercial: Legal Issues and Contracts: Selling |
| Scrum Master | Facilitate meetings (daily scrum, sprint planning, sprint demo and retrospective): Shielding the team: Conflict resolutions: Coaching Agile practices: Tight grip on Agile Estimating and planning: Forecasting: Removing impediments: Creating communication channel (team, Product owner, stakeholder): Servant leader: Enforcing Rules| |
| CTO/CT | x| |
| SME’s – Dev/QA |
| SME’s – Emerg Tech |
| Tribe Lead |
| Chapter Lead / Culture | Coordination in a discipline or competence happens in ' chapters '; people from different teams (squads) of a particular discipline or competence meet regularly. You can think of IT engineers with front-end expertise and customer journey experts with intermediate specialisation. The chapter lead (CL) is responsible for the performance cycle and for the personal development of chapter members on their journey to mastery. This is pretty remarkable because when you approach this from the performance point of view and you would expect this would be a problemowners’ task. However in the agile matrix organization, this is consciously separated. This requires the product owner to choose for his or her craftsmanship and prioritize it (work from the chapter should be prioritized through the PO; ‘the chapter ' does not work '). The chapter lead carries out its leadership tasks next to his or her duties in his or her own squad, say 40% of his/her time. |
| Reporting & Knwld Mgnt |
| Talent Acquisition |
| Resource Broker |
| Contracts/Legal/Pricing |
| Digital Innovation Hub |
| Digital Build Leader |
