## Design Principles Approval Form

This form is to capture the approval from the CTO's for Industry, Offering, Delivery and Regions (inc Solution). Each CTO will be responsible for reading and agreeing in principle that the Design Principles are DXCs Guidance to our technologist community.


#### Approval Table

Status = Approved / Not Approved

<b>Approved</b> = You recognise and promote All / Majority of these principles across your organisation.

<b>Not Approved</b> = You do not recognise any these principles in the operation of your organisation.



| Name | Business Unit Covered | Status |
|:------|:----------------------|:--------|
| Dan Hushon | DXC | :grey_question: |
| Carl Kinson | Architecture Office | Approved :+1: |
| Chris Swan | Delivery | :grey_question: |
| Taber West | Solution | :grey_question: |
| Femi Ladega | Healthcare and Life Sciences | :grey_question: |
| Brian Wallace | Insurance | :grey_question: |
| Venkataman Bala | Banking | :grey_question: |
| TBC | Travel and Transportation | :grey_question: |
| Chris Fangmann | Manufacturing | :grey_question: |
| Michael Conlins   | USPS | :grey_question: |
| Stephen Singh | AMS | :grey_question: |
| Daniel Angelucci | APAC | :grey_question: |
| Daniel Bondi | AUZ | :grey_question: |
| Melih Yener | NCE | :grey_question: |
| Juan Juan | S&W | :grey_question: |
| Sukhi Gill | UK&I | :grey_question: |
| Dragan Rakovich | Analytics | :grey_question: |
| JP Morgenthal | Application Svcs | :grey_question: |
| TBC |  BPS | :grey_question: |
| Jim Miller | Cloud and Platform Services | :grey_question: |
| Chris Nokkentved | Enterprise and Cloud Applications | :grey_question: |
| Marc Wilkinson | Modern Workplace & Mobility | :grey_question: |
| Chris Moyer | Security | :grey_question: |
