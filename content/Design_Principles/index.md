![Architecture Office](/static/images/dxclogo.png)

# Design Principles

DXC Technology’s design principles guide DXC technologists and partners who are designing or developing a DXC product, offering, solution or service. The design principles also offer a look into the heart and soul of DXC — how we work and approach problems. Technologists should understand the principles and adhere to them in their projects.

### Technical Doctrine

Many of these principles are being enacted through the [DXC - Technical Doctrine](https://github.dxc.com/pages/DXC-OCTO/DXC-Technical-Doctrine/). Please visit the link to check the latest developments and help us improve DXC through best practice.

### What are design principles?
Design principles are sets of generally applicable standards, guidelines, human biases, and design considerations, all of which reflect the accumulated knowledge and experience of practitioners and researchers. They serve as a starting point for the creation of new designs to solve problems. Design principles usually combine developments across all design-related disciplines, including behavioral, technical and process activities.

### How do I use the design principles?
DXC‘s design principles provide high-level guidance and clarity to our technologists on how DXC expects them to design and develop. The development teams will decide how they want to implement the principles. In some cases, DXC will provide standards to support the principle.

Our 20 design principles are managed by DXC’s Office of the CTO (OCTO). Anyone in DXC can suggest changes or additions to the design principles; these will be vetted and approved by the OCTO Architecture Office. Submit your suggestions here.

Note: We will use GitHub for controlling the master set of principles. This will enable us to handle changes (pull requests) and issues centrally and collaboratively, follow the [Contribution Doc](https://github.dxc.com/ArchitectureOffice/Standards/blob/master/CONTRIBUTING.md) at the root of this repo.

### Actors use of Design Principles

| Actor | Usage Description | Focused Principles |
|------|-------------------|---------------------|
| Product Owner| The Product Owner will be responsible in ensuring that the design principles are followed and used to enable and enhance the product they are responsible for. Products developed under these principles will be market leading and should achieve the differentiated products DXC expects. |  ALL |
| Developer    | The Developer should adhere to these principles in the development of the product, only by exception should the product be developed outside the principles. Some of the principles outline how the team should work and operate, developers are responsible for ensuring coherence. | ALL |
| Architect    | The Architect should design the product in alignment with Principles working with the developer and team to understand the architecture aspects that should be captured to achieve the principles which align to design and engineering considerations. | ALL |
| Tester       | Tester SME should observe the principles which impact their role within the development team, the principles outline a way of working as well which needs to adhered too. | [9](#9-testing-testing-testing),[11](#11-no-i-in-team) |
| Security     | Security SME should observe the principles which impact their role within the development team, the principles outline a way of working as well which needs to adhered too. | [10](#10-no-i-in-team),[7](#7-security-from-day-one) |
| Project Mgr   | Project Mgr should observe the principles which impact their role within the development team, the principles outline a way of working as well which needs to adhered too. | [6](#6-be-agile),[10](#10-no-i-in-team),[4](#4-giver-and-receiver-of-feedback),[11](#11-fail-fast-fail-cheap-but-learn-faster) |
| Operations   | Operations ensures that the delivery of the service continues without unexpected interruption. | [5](#5-automation-built-in),[1](#1-be-open),[6](#6-be-agile),[10](#10-no-i-in-team),[4](#4-giver-and-receiver-of-feedback),[18](#18-information-enabled),[8](#8-design-for-operations) |

**Note** These are role profiles rather than "jobs" as it is fully expected, using the revised competency frameworks that individuals will be multi-disciplined ("X-shaped") rather than siloed.

-------------

## DXC Design Principles

### 1: Be Open

Where possible, open up the design to collaborative input; this can be just elements or the entire design, depending on your circumstances. The use of open source products is also supported and encouraged to facilitate collaborative development.

- Consider using open source products; “don’t reinvent the wheel” (principle 10).
- Consider contributing to open source projects. See DXC’s [OpenSource Guidelines](https://dxcportal.sharepoint.com/sites/opensource/SitePages/Home.aspx).

DXC operates a process for using open source and for contributing to open source (detailed in the White List and Green List, respectively).

### 2: Keep it simple
Do not over-complicate or over-engineer the design in both the construct and its operation. Complexity of a solution (if required) will be achieved via an iterative approach. If the design problem is complex, decompose it into small components.

### 3: Transparency / Visibility
As you design and develop, transparency and visibility are important. As you use information and data and report progress, other programs will be dependent on your output.

- Let people know what you are doing with their data. Consider how your design can respond to Freedom of Information or Data Protection requests. (This is supported by the “security from Day One” principle.)

- Provide visibility to users and others in a consistent and open way. Users should not be expected to pull status data as an offline report.

### 4: Giver and Receiver of Feedback
Ensure you are both giving and receiving feedback. This is part of the iterative process. You must encourage feedback as being constructive; although some feedback you may not want to hear, you should acknowledge it. You should also provide feedback into other designs, especially if you are dependent on them. Always communicate honestly.

### 5: Automation Built-In
All designs should maximize automation for deployment, configuration, operation and execution. There are two dimensions to this principle:

- Emphasize design choices that enable configuration control to ensure consistency among physical and logical assets in all environments (dev, test and production).

- Enable the management of all assets and activities “as code” by externalizing policy attributes, as well as governance/control objectives and requirements, as configuration items.

The term “clever lazy people” should resonate with this principle.

### 6: Be Agile
How you design, and what you are designing, need to be iterative. This will provide visible results quicker, help to continuously validate the direction, and de-risk the end result.

Speed is a priority over completeness. Products and services will be designed with speed as a priority.

- Designing – This should be done in the smallest, most iterative steps possible. Think: Minimum Viable Product (MVP) –> Limited Market/Functionality –> General Market/Functionality

- The Design – Consider the development process such that features and functions can be enabled iteratively. Whenever possible, ensure the right feedback mechanisms are in place for the customer to be a part of this process to build relationship, client investment and transparency.

- Development – Break development down into small, executable chunks; take ownership of your deliverables.

### 7: Security from Day One
Treat having good security as a basic requirement. Adhere to a [least privilege model](https://en.wikipedia.org/wiki/Principle_of_least_privilege); consider who and what needs access to data. Assume personal data is sensitive unless explicit permission is provided.

### 8: Design for Operations
Designs need to support our new operations model, which requires ensuring that the delivery aspect of the solution meets high-performing operations needs and supports the continuous integration/continuous delivery (CI/CD) movement.

- See [High Performing Operations Characteristics](../High_Performing_Operations).

- Be sure to design for failure (Chaos Monkey). Also, avoid single points of failure!

- To support continuous development and delivery, the design phase needs to provide more machine-interpretable documentation — i.e., Architecture as Code. This supports High Performing Operations. See the [Dynamic Document Framework](https://github.dxc.com/ArchitectureOffice/DDF).

### 9: Design to the Customer / Business.
Ensure that you have the target user in mind when designing. Ensure you meet the needs of the user while maintaining a balance of “design to price” and “keep it simple.” Engage the customer when possible. The customer experience (not to be confused with UX/UI — see principle 15) is paramount.

- Understand the problem – Spend time obtaining a deep understanding of the client business, including broader trends in the industry and region. Immerse yourself. Business goals should be your goals in the design process. Client pain should be your pain. Basic knowledge is acceptable at a high-level, but domain expertise and full comprehension of the client objectives or mission are better.

### 10: No 'I' in Team
In the design and development process, ensure you are part of a team in which each member can input, test and execute various aspects of the design. See [High Performing Teams](../High_Performing_Teams/).

### 11: Fail Fast, Fail Cheap, but Learn Faster
Realize quickly if an activity is not working. Understand why the failure has happened and learn from it as soon as possible. Instances to challenge the “fail fast, fail cheap” principle are:

- When there is significant investment already in place by the client and success is tied to the client brand as a result

- When you are applying an already-proven concept to a new but known problem

- When the client problem is complex enough that failing fast would destabilize the client business

### 12: Smaller is better

Breaking your design down into smaller, more manageable chunks will support many downstream processes including Agile and iterative development. Single business function-based services are the key to achieving this.

Move to microservice architecture. The process of decomposing a monolithic solution into smaller domains is a useful way to size the individual domains to build a reference [Microservice Pattern](../Microservices/Microservice-Pattern.md). Also review [Decomposing in Sub Domains](../Microservices/Decomposing%20by%20Sub%20Domains.md).

- <u>Scaling</u> would appear to be an anti-pattern to this principle but designing the ability to scale your solution is a critical aspect. Consumption of the solution needs to be understood and considering how you scale horizontally by adding more pools or resources, or vertically by adding capacity to existing systems.  Vertical scaling typically has limitations, and these should be understood before committing to one method.

### 13: Be Loosely Coupled
The design should encourage loose coupling of components; do not have tightly coupled dependencies in the design. This principle is supported by the “smaller is better” and “easy to integrate” principles. Wherever possible, apply a stateless/share-nothing architecture.

### 14: Easy to Integrate
Provide easy integration, clearly defining the interfaces and supporting both data and control flow. This includes ensuring you integrate with known common products or services that your design will have to work with on a regular basis.

- APIs should be used to connect services. Where appropriate, open APIs are preferred. [API Guidelines](https://github.dxc.com/ArchitectureOffice/api-design-guidelines/blob/master/Guidelines.md).
- Integration should be considered across both processes and people.

### 15: All about UI/UX
The User Interface (UI) and User Experience (UX) are critical aspects of the design of any system. Focus on the look, feel and operation of the system (UI), and maintaining interest and creating a positive experience in the usage of the system (UX). The latter is as critical as the UI, if not moreso.

Responsiveness is key to adoption, use and user satisfaction.

DXC has developed a set of UI guidelines; see [DXC UI Guidelines](https://dxcportal.sharepoint.com/:w:/r/sites/uiTechnologyDualDesign/UIdesign/_layouts/15/WopiFrame.aspx?sourcedoc=%7B24FFD5AD-C11E-49E0-9BB9-5F9119644A6E%7D&file=OC_Visual_Interface.docx&action=default&IsList=1&ListId=%7B95C94414-A09F-4F35-9CD9-DA4E7900012D%7D&ListItemId=11).

### 16: Testing, Testing, Testing
Do not wait until the end of the process to test your current state. Be it code or general artefacts, test with defined user parameters or peer reviews.

### 17: Clear versioning
With depreciation periods allowing the organization to advance without needing to conduct release management across large scopes.

All services and APIs must show clear versioning. This includes depreciation periods, allowing the organization to advance without needing to conduct release management across large scopes.

When to Deprecate an API

- 1: The API is Insecure
- 2: the API has too many bugs
- 3: The API does not support important use cases
- 4: The API is inefficient

### 18: Information Enabled
There are two dimensions to this principle:

- Information – Use information to validate the design; do not guess at needs and requirements. Gather data on behavior and needs to validate and justify the design.

- Analytics – Either build in or provide data feeds to support ongoing analysis of the product. This can be used for both operational delivery support and as a feedback loop to support continuous development. Where possible, enable input from analytics to help guide operation of the product.

### 19: Design to Price
Designs should meet market or target pricing. This may mean limiting the initial scope and functionality of the design. If nobody buys it because it’s too expensive, why have it?

- Know your licensing – If you are using, re-using or developing for re-use, consider the licensing implications.

- Know your buying behaviour – Ensure the design can support how the product/services will be bought. For example, as-a-service solutions need to support granular procurement of units of service.

### 20: Don't Re-invent the Wheel
Where existing services or products exist, you should adopt them. Focus attention on the aspects of the design that cannot be delivered by other products and services. Copy+Paste is not a crime, though you should notify people when you have done this.

## Summary

To summarise the 20 DXC Principles are there to guide and encourage our technologist to think and operate differently. They support the transformation DXC want to see in how we Build, Sell and Deliver Services and Solutions for our clients. Each principle will resonate differently for various people and roles. We have chosen to keep these at a macro level, as each Business Unit, Group, Team or individual can apply them to their working environment.

----------------------

## Supporting Standards / Guidelines Links

- [DXC DevOps](../DevOps)

- [DXC API Guidelines](../API_Standards/Guidelines.md)

- [DXC Mobile Applications Development Guidelines](../Mobile/DXC%20mobile%20apps%20standards.md)

- [DXC Inner Source Guidelines](../Inner_Source/README.md)

- [DXC DevOps](../DevOps)

- [DXC UI Guidelines](https://dxcportal.sharepoint.com/:w:/r/sites/uiTechnologyDualDesign/UIdesign/_layouts/15/WopiFrame.aspx?sourcedoc=%7B24FFD5AD-C11E-49E0-9BB9-5F9119644A6E%7D&file=OC_Visual_Interface.docx&action=default&IsList=1&ListId=%7B95C94414-A09F-4F35-9CD9-DA4E7900012D%7D&ListItemId=11) (This is a SharePoint site, access may need to be provided by [Karen Russell](mailto:krusse24@csc.com))

- [DXC Brand Central](https://my.dxc.com/brand-guide.html)

- [DXC Opensource Program](https://dxcportal.sharepoint.com/sites/opensource/SitePages/Home.aspx)

- [DXC High Performing Operations](../High_Performing_Operations)
