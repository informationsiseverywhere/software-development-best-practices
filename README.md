# DXC Standards

:earth_americas: Go the site: https://github.dxc.com/pages/ArchitectureOffice/Standards/

# March 5th Update
> The repo structure has been changed in preparation for enhancing the way the content is navigated shortly.  
> If you are yet to update your local fork, perform the following command in **git bash**, or if you have a familiar way of updating your fork, that's fine too!  
```shell
$ git remote add upstream git@github.dxc.com:ArchitectureOffice/Standards.git
$ git pull upstream master
$ git push origin master
```

 > If you are already in the process of adding some edits then you can add the `--rebase` option to the `pull` command to try to incorporate your in-flight changes in your fork.  

### New content layout

 > The new layout is shown below - the directories, and your content,  that were hung off the root of the repo are now below `content/`  
 > All images should now be stored in `static/images` and references in your contents as  
 > `![alt-image-text](/static/images/example_image.png)`.

 > Any queries - drop an email to `architectureoffice@dxc.com`

```shell
. (repo root)
|___ content
|   |___
|   |___ example_standards_directory
|        |___ example_markdown_content.md
|___ static
|    |___ images
|         |___ example_image.png
|___ README.md
|___ CONTRIBUTING.md
```



### Links to Working Areas

- [Design Disciplines](content/Design%20Principles)
- [API Design Guidelines](content/API_Standards)
- [DevOps](content/DevOps)
- [Inner Source](content/Inner_Source): using Open Source best practices and culture within DXC
- [Mobile](content/Mobile)
- [Modelling](content/Modelling)
- [Offerings](content/Offerings)
- [High Performing Operations](content/High_Performing_Operations)
- [High Performing Teams](content/High_Performing_Teams)

### Contributing

See [CONTRIBUTING.md](CONTRIBUTING.md).
